# NodeJS+ExpressJS for API Server

Architecture Stack: NodeJS + ExpressJS + TypeScript + Postgresql

## Install packages

npm install

## Dev

npm run dev

## Prod

npm run build
npm run start
[TODO]

# Useful Resources

### NodeJS Framework - Express

https://expressjs.com/en/4x/api.html

### Database - Postgresql & related tutorials

https://www.postgresql.org
https://postgres.fun

### TypeScript

https://www.tslang.cn
https://www.typescriptlang.org

### ORM - sequelize sequelize-typescript

https://www.sequelize.com.cn
https://sequelize.org
https://www.npmjs.com/package/sequelize-typescript

### Authorization Token (JWT) & related examples

https://www.npmjs.com/package/jsonwebtoken
https://blog.csdn.net/wwww001001/article/details/83381683
https://www.npmjs.com/package/bcrypt

### Multi-Language Support - i18n

https://www.npmjs.com/package/i18n

### Logger - Winston

https://www.npmjs.com/package/winston

### Date & Time - Moment & Timezone

http://momentjs.cn
http://momentjs.cn/timezone/docs/

### validator - express-validator

https://express-validator.github.io/docs/
https://www.npmjs.com/package/validator

