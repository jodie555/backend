'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('vehicles', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
      },
      plate_number: { type: Sequelize.STRING },
      is_common: { type: Sequelize.BOOLEAN },
      billing_method: { type: Sequelize.STRING },
      plate_type: { type: Sequelize.STRING },
      status: { type: Sequelize.STRING },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
      deleted_at: { type: Sequelize.DATE },
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('vehicles')
  },
}
