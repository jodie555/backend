'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('transaction_records', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
      },
      transaction_type: { type: Sequelize.STRING },
      parking_record_id: { type: Sequelize.UUID },
      monthly_record_id: { type: Sequelize.UUID },
      other_record_id: { type: Sequelize.UUID },
      amount: { type: Sequelize.DECIMAL(11, 2) },
      payment_method: { type: Sequelize.STRING },
      trace_number: { type: Sequelize.STRING },
      receipt_number: { type: Sequelize.STRING },
      index: { type: Sequelize.STRING },
      remarks: { type: Sequelize.STRING },
      has_settled: { type: Sequelize.BOOLEAN },
      alert: { type: Sequelize.BOOLEAN },
      status: { type: Sequelize.STRING },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
      deleted_at: { type: Sequelize.DATE },
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('transaction_records')
  },
}
