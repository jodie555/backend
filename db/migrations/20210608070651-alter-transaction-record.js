'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.renameColumn(
      'transaction_records',
      'amount',
      'paid_amount'
    )
    await queryInterface.addColumn('transaction_records', 'payable_amount', {
      type: Sequelize.DECIMAL(11, 2),
    })
    await queryInterface.addColumn('transaction_records', 'discount_amount', {
      type: Sequelize.DECIMAL(11, 2),
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('transaction_records', 'discount_amount')
    await queryInterface.removeColumn('transaction_records', 'payable_amount')
    await queryInterface.renameColumn(
      'transaction_records',
      'paid_amount',
      'amount'
    )
  },
}
