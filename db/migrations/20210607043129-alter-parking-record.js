'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('parking_records', 'billing_method', {
      type: Sequelize.STRING,
    })
    await queryInterface.addColumn('parking_records', 'plate_type', {
      type: Sequelize.STRING,
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('parking_records', 'plate_type')
    await queryInterface.removeColumn('parking_records', 'billing_method')
  },
}
