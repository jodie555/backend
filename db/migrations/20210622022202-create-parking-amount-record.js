'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('parking_amount_records', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
      },
      parking_record_id: { type: Sequelize.UUID },
      start_time: { type: Sequelize.DATE },
      end_time: { type: Sequelize.DATE },
      rate_per_time_unit: { type: Sequelize.INTEGER },
      time_unit: { type: Sequelize.INTEGER },
      number_of_time_unit: { type: Sequelize.INTEGER },
      amount: { type: Sequelize.DECIMAL(11, 2) },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
      deleted_at: { type: Sequelize.DATE },
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
}
