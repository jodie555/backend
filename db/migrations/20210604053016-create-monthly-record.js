'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('monthly_records', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
      },
      vehicle_id: { type: Sequelize.UUID },
      start_date: { type: Sequelize.DATE },
      end_date: { type: Sequelize.DATE },
      parking_lot: { type: Sequelize.STRING },
      name: { type: Sequelize.STRING },
      mobile: { type: Sequelize.STRING },
      payment_date: { type: Sequelize.DATE },
      remarks: { type: Sequelize.TEXT },
      status: { type: Sequelize.STRING },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
      deleted_at: { type: Sequelize.DATE },
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('monthly_records')
  },
}
