'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('price_sessions', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
      },
      price_type_id: { type: Sequelize.UUID },
      start_time: { type: Sequelize.TIME },
      end_time: { type: Sequelize.TIME },
      rate_per_time_unit: { type: Sequelize.INTEGER },
      max_rate_per_session: { type: Sequelize.INTEGER },
      grace_period_per_time_unit: { type: Sequelize.INTEGER },
      grace_period_per_session_unit: { type: Sequelize.INTEGER },
      remarks: { type: Sequelize.TEXT },
      status: { type: Sequelize.STRING },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
      deleted_at: { type: Sequelize.DATE },
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('price_sessions')
  },
}
