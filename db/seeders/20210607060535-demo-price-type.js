'use strict'
const { v4: uuidv4 } = require('uuid')

module.exports = {
  up: async function (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('price_schemes', [
      {
        id: '2cecba89-ff13-46a3-81d4-62352ae9f1d0',
        time_unit: 60,
        session_number: 2,
        plate_type: 'p',
        remarks: 'Private car/van as per',
        status: 'a',
        index: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '4a10ce95-e542-4578-b92b-ea8654c7f0ee',
        time_unit: 60,
        session_number: 2,
        plate_type: 'm',
        remarks: 'Bike as per',
        status: 'a',
        index: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '7a902776-0d9e-4b66-a9fe-5f68c579e031',
        time_unit: 15,
        session_number: 3,
        plate_type: 'd',
        remarks: 'Drop off as per',
        status: 'a',
        index: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ])
  },
  down: async function (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('price_types', null, {})
  },
}
