'use strict'

let bcrypt = require('bcryptjs')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('users', [
      {
        id: 'a34d8713-43e5-43cf-b2f5-99a4c2ba8c26',
        username: 'admin',
        password: bcrypt.hashSync('123456', bcrypt.genSaltSync(10)),
        role: 'a',
        status: 'a',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '56e010d3-d57e-4e9f-941d-8baa8241dfcd',
        username: 'back-office',
        password: bcrypt.hashSync('123456', bcrypt.genSaltSync(10)),
        role: 'b',
        status: 'a',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 'a48020cd-fd7b-4800-a3e1-d74ab3dbec49',
        username: 'operator',
        password: bcrypt.hashSync('123456', bcrypt.genSaltSync(10)),
        role: 'o',
        status: 'a',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {})
  },
}
