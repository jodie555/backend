'use strict'
const { v4: uuidv4 } = require('uuid')

module.exports = {
  up: async function (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('price_sessions', [
      {
        id: uuidv4(),
        price_scheme_id: '2cecba89-ff13-46a3-81d4-62352ae9f1d0',
        start_time: '08:00',
        end_time: '20:00',
        rate_per_time_unit: 20,
        max_rate_per_session: 130,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 60,
        remarks: '',
        status: 'a',
        index: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '2cecba89-ff13-46a3-81d4-62352ae9f1d0',
        start_time: '20:00',
        end_time: '08:00',
        rate_per_time_unit: 20,
        max_rate_per_session: 130,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 60,
        remarks: '',
        status: 'a',
        index: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '4a10ce95-e542-4578-b92b-ea8654c7f0ee',
        start_time: '08:00',
        end_time: '20:00',
        rate_per_time_unit: 10,
        max_rate_per_session: 70,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 60,
        remarks: '',
        status: 'a',
        index: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '4a10ce95-e542-4578-b92b-ea8654c7f0ee',
        start_time: '20:00',
        end_time: '08:00',
        rate_per_time_unit: 10,
        max_rate_per_session: 70,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 60,
        remarks: '',
        status: 'a',
        index: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '7a902776-0d9e-4b66-a9fe-5f68c579e031',
        start_time: '08:00',
        end_time: '11:00',
        rate_per_time_unit: 15,
        max_rate_per_session: 150,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 10,
        remarks: '',
        status: 'a',
        index: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '7a902776-0d9e-4b66-a9fe-5f68c579e031',
        start_time: '11:00',
        end_time: '18:00',
        rate_per_time_unit: 20,
        max_rate_per_session: 500,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 10,
        remarks: '',
        status: 'a',
        index: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: uuidv4(),
        price_scheme_id: '7a902776-0d9e-4b66-a9fe-5f68c579e031',
        start_time: '18:00',
        end_time: '08:00',
        rate_per_time_unit: 10,
        max_rate_per_session: 150,
        grace_period_per_time_unit: 5,
        grace_period_per_session_unit: 10,
        remarks: '',
        status: 'a',
        index: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ])
  },
  down: async function (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('price_sessions', null, {})
  },
}
