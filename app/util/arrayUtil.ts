'use strict'

export {}

declare global {
  namespace NodeJS {
    interface Global {
      arrUnique: Function
      isArray: Function
      flatten: Function
      arrKeyValueFiter: Function
    }
  }
}

declare global {
  interface Array<T> {
    unique: Function
    array_filter: Function
  }
}

global.arrUnique = function (arr) {
  return Array.from(new Set(arr))
}

global.isArray = function (arr) {
  return Object.prototype.toString.call(arr) === '[object Array]'
}

Array.prototype.unique = function () {
  return Array.from(new Set(this))
}

Array.prototype.array_filter = function () {
  return this.filter((i) => i)
}

global.flatten = function (arr) {
  return [].concat(
    ...arr.map((x) => (Array.isArray(x) ? global.flatten(x) : x))
  )
}

global.arrKeyValueFiter = function (arr, key) {
  let res = new Map()
  let newArr = arr.filter(
    (item) => !res.has(item[key]) && res.set(item[key], 1)
  )
  return newArr
}
