'use strict'

const fs = require('fs')
const https = require('https')

exports.downImg = async function (url, path) {
  return new Promise((resolve, reject) => {
    https.get(url, function (res) {
      let data = ''
      res.setEncoding('binary')
      res.on('data', function (chunk) {
        data += chunk
      })
      res.on('end', function () {
        fs.writeFileSync(path, data, 'binary', function (err) {
          console.log(err)
          reject()
        })
        resolve(path)
      })
    })
  })
}
