'use strict'

// var logger = require('../logger')
import logger from '../logger'
import { getLocale } from 'i18n'

function addVirtualTranslation(fieldName, model) {
  const locale = getLocale()
  logger.info('[locale:' + locale + ']')
  switch (locale) {
    case 'en':
      return model[fieldName + '_en'] || model[fieldName + '_zh_tw'] || ''
    case 'zh-TW':
      return model[fieldName + '_zh_tw'] || model[fieldName + '_en'] || ''
    default:
      return model[fieldName + '_zh_tw'] || model[fieldName + '_en'] || ''
  }
}

declare global {
  namespace NodeJS {
    interface Global {
      addVirtualTranslation: Function
    }
  }
}

global.addVirtualTranslation = addVirtualTranslation
