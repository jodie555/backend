'use strict'

export {}

declare global {
  namespace NodeJS {
    interface Global {
      isEmpty: Function
    }
  }
}
global.isEmpty = function isEmpty(data) {
  var dataType = typeof data //typeof returns a string. There are six possibilities：'number'、'string'、'boolean'、'object'、'function'、'undefined';
  switch (dataType) {
    case 'number':
      return false
    case 'string':
      if (data == '') {
        return true
      } else {
        return false
      }
    case 'boolean':
      return data
    case 'object':
      if (!data && typeof data != 'undefined' && data != 0) {
        return true
      } else {
        for (var key in data) {
          //Non empty object
          return false
        }
        return true
      }
    case 'function':
      return false
    case 'undefined': //undefined
      return true
  }
}
