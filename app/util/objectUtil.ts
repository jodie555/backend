'use strict'

export {}

declare global {
  namespace NodeJS {
    interface Global {
      isObject: Function
      compareObj: Function
    }
  }
}

global.isObject = function (obj) {
  return Object.prototype.toString.call(obj) === '[object Object]'
}

global.compareObj = function (objA, objB) {
  var aProps = Object.getOwnPropertyNames(objA)
  var bProps = Object.getOwnPropertyNames(objB)
  if (aProps.length != bProps.length) {
    return false
  }
  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i]

    var propA = objA[propName]
    var propB = objB[propName]
    if (typeof propA === 'object') {
      if (!this.isObjectValueEqual(propA, propB)) {
        return false
      }
    } else if (propA !== propB) {
      return false
    }
  }
  return true
}
