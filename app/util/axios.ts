'use strict'
import axios, { Method } from 'axios'
import { Md5 } from 'md5-typescript'
import moment from 'moment-timezone'
import { CONSTANT_ERROR, Exception } from '../exception/Exception'
import logger from '../logger'

export async function callCloudApi(
  url: string,
  method: Method,
  params: object,
  data: object,
  header?: object
) {
  logger.info(
    `[call cloud api]-[url:${process.env.CLOUD_URI}${url}]-[method:${method}]-[params:${params}]-[body:${data}]`
  )
  let requestTime = moment().format()
  let nonce = Math.random().toString()
  let secret = Md5.init(
    process.env.CLOUD_APP_SECRET +
      process.env.CLOUD_APP_SALT +
      requestTime +
      nonce
  )
  let token = null
  if (global['authorization']) {
    token = global['authorization']
  }
  let result: any
  try {
    result = await axios({
      method: method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'app-id': process.env.CLOUD_APP_ID,
        'request-time': requestTime,
        nonce: nonce,
        secret: secret,
        Authorization: token,
      },
      url: `${process.env.CLOUD_URI}${url}`,
      data: data,
      params: params,
    })
      .then((res) => {
        return res
      })
      .catch((err) => {
        logger.error(err)
      })
  } catch (err) {
    throw new Exception(
      CONSTANT_ERROR.CALL_CLOUD_API_ERROR.message,
      CONSTANT_ERROR.CALL_CLOUD_API_ERROR.code
    ).setDetail(err.message)
  }
  if (!result || result.status != 200 || !result.data) {
    throw new Exception(
      CONSTANT_ERROR.CALL_CLOUD_API_ERROR.message,
      CONSTANT_ERROR.CALL_CLOUD_API_ERROR.code
    )
  }
  return result.data
}
