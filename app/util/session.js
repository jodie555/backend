const FastifyCookie = require("fastify-cookie");
const FastifySession = require("fastify-session");

const sessionSecret =
    "y8gr8nby1gn3iegryn1iy83or81gdr8or31dg3oy1rgoy1degrody1eg";

module.exports = fastify => {
    fastify.register(FastifyCookie);
    fastify.register(FastifySession, {
        secret: sessionSecret,
        name: "session",
        maxAge: 2 * 60 * 60 * 1000,
        cookie: {
            secure: false,
        },
    });
};
