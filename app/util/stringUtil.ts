'use strict'

export {}
declare global {
  namespace NodeJS {
    interface Global {
      btoa: Function
      atob: Function
    }
  }
}

declare global {
  interface String {
    ucfirst: Function
    ucwords: Function
    lcfirst: Function
    camel: Function
    studly: Function
    snake: Function
    trims: Function
    trimsLeft: Function
    trimsRight: Function
    paraphrase: Function
  }
}
/**
 * The first word is capitalized
 * @returns {string}
 */
String.prototype.ucfirst = function () {
  return this.substring(0, 1).toUpperCase() + this.substring(1)
}
/**
 * Each word is capitalized
 * @returns {string}
 */
String.prototype.ucwords = function () {
  return this.replace(/\b[a-z]/g, (c) => c.toUpperCase())
}
/**
 * The first word is lowercase
 * @returns {string}
 */
String.prototype.lcfirst = function () {
  return this.replace(/\w/, (c) => c.toLowerCase())
}
/**
 * Big hump character
 * @returns {string}
 */
String.prototype.camel = function () {
  return this.replace(/[-_]/g, ' ').ucwords().replace(/ /g, '')
}
/**
 * Small hump character
 * @returns {string}
 */
String.prototype.studly = function () {
  return this.camel().lcfirst()
}
/**
 * Hump character to separator character
 * @param delimiter 分隔符
 * @returns {string}
 */
String.prototype.snake = function (delimiter = '_') {
  return this.replace(/(.)(?=[A-Z])/g, (c) => {
    return c.toLowerCase() + delimiter
  }).toLowerCase()
}
/**
 * Remove the characters entered on both sides
 * @param flags
 * @returns {string}
 */
String.prototype.trims = function (flags = '/') {
  return this.replace(new RegExp(`^[${flags}]*(.*?)[${flags}]*$`, 'g'), '$1')
}
/**
 * Remove the input character on the left
 * @param flags
 * @returns {string}
 */
String.prototype.trimsLeft = function (flags = '/') {
  return this.replace(new RegExp(`^[${flags}]*(.*?)$`, 'g'), '$1')
}
/**
 * Remove the input character on the right
 * @param flags
 * @returns {string}
 */
String.prototype.trimsRight = function (flags = '/') {
  return this.replace(new RegExp(`(.*?)[${flags}]*$`, 'g'), '$1')
}

String.prototype.paraphrase = function () {
  // var str = this.replace(/(^\s*)|(\s*$)/g, '')
  var str = this
  if (/(\+|\-|\&|\||\!|\(|\)|\{|\}|\[|\]|\^|\”|\~|\*|\?|\:|\/|\\)/g.test(str)) {
    var matchs = str.match(
      /(\+|\-|\&|\||\!|\(|\)|\{|\}|\[|\]|\^|\”|\~|\*|\?|\:|\/|\\)/g
    )
    matchs.unique().forEach((el) => {
      str = str.replace(el, '\\' + el)
    })
  }
  return str
}

global.btoa = function (str) {
  return new Buffer(str).toString('base64')
}

global.atob = function (str) {
  return new Buffer(str, 'base64').toString()
}
