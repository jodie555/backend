'use strict'

export {}

declare global {
  namespace NodeJS {
    interface Global {
      setRouteMacro: Function
    }
  }
}

function resourceMacro(router) {
  router.resource = function (resource, controller) {
    resource = resource.trims()
    controller = require(global.app_path(
      'controllers/' +
        (global.isEmpty(controller)
          ? resource.studly() + 'Controller'
          : controller)
    ))
    router
      .route('/' + resource.snake() + 's')
      .get(controller.index)
      .post(controller.create)
    router
      .route('/' + resource.snake() + 's/:' + resource.studly())
      .get(controller.read)
      .put(controller.update)
      .delete(controller.delete)
  }
}

global.setRouteMacro = function (router) {
  resourceMacro(router)
  // groupMacro(router)
  // middlewareMacro(router)
}
