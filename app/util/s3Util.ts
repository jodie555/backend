'use strict'

// var aws = require('aws-sdk')
import aws from 'aws-sdk'
import { CONSTANT_ERROR, Exception } from '../exception/Exception'
var s3 = new aws.S3()
var sts = new aws.STS()
import mime from 'mime'
// require('../app')
import '../app'

var myBucket = process.env.AWS_BUCKET
aws.config.accessKeyId = process.env.AWS_ACCESS_KEY_ID
aws.config.secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
aws.config.region = process.env.AWS_DEFAULT_REGION

export let getTemporarySessionToken = async function () {
  return await sts
    .getSessionToken({ DurationSeconds: 86400 })
    .promise()
    .then((res) => {
      return res.Credentials
    })
    .catch((err) => {
      throw new Exception(err.message, err.code)
    })
}

export let getSignedUrl = function (path) {
  if (!path) {
    return ''
  }
  return s3.getSignedUrl('getObject', {
    Bucket: myBucket,
    Key: path,
    ResponseContentType: mime.getType(path),
  })
}

export let url2path = function (url) {
  // Short path  images/default.png
  if (url.indexOf('http') === -1) {
    return url
  }

  // Compressed path  https://drejkshzkxfna.cloudfront.net/eyJidWNrZXQiOiJzNGEtaGsiLCJrZXkiOiJpbWFnZXMvZGVmYXVsdC5wbmciLCJyZXNpemUiOnsid2lkdGgiOjUxMiwiaGVpZ2h0IjpudWxsLCJmaXQiOiJpbnNpZGUiLCJ3aXRob3V0RW5sYXJnZW1lbnQiOmZhbHNlfX0=
  let imageHandleApiIndex = url.indexOf(process.env.AWS_IMAGE_HANDLER_API)
  if (imageHandleApiIndex === 0) {
    let encodeStr = url.substr(process.env.AWS_IMAGE_HANDLER_API.length + 1)
    console.log(encodeStr)
    var request = Buffer.from(encodeStr, 'base64').toString()
    return JSON.parse(request)['key']
  }

  // Encrypted path https://s4a-hk.s3.amazonaws.com/images/default.png?AWSAccessKeyId=AKIA5CLP6K6AW2BPVMPV&Expires=1573727210&Signature=puWC6CaJAb3g%2FrqMzfuHREb3FSE%3D&response-content-type=image%2Fpng
  let prefixIndex = url.indexOf(process.env.PREFIX_URL)
  if (prefixIndex === 0) {
    return url.substring(process.env.PREFIX_URL.length, url.indexOf('?'))
  }

  // None of the above matches throwing an exception
  throw new Exception(
    CONSTANT_ERROR.PATH_IS_ERROR.message,
    CONSTANT_ERROR.PATH_IS_ERROR.code
  )
}

export let getAbbrPath = function (path, width) {
  if (!width) {
    width = 300
  }
  const imageRequest = JSON.stringify({
    bucket: process.env.AWS_BUCKET,
    key: path,
    edits: {
      resize: {
        width: parseInt(width),
        height: null,
        options: {
          fit: 'inside',
          withoutEnlargement: false,
        },
      },
    },
  })
  var url = Buffer.from(imageRequest).toString('base64')
  return process.env.AWS_IMAGE_HANDLER_API + '/' + url
}

export let sampleEncode = function (str) {
  return new Buffer(
    str
      .split('')
      .map(function (str) {
        return str.charCodeAt()
      })
      .map(function (ord) {
        var str = parseInt(ord).toString(36)
        if (str.length < 2) {
          str = (Array(2).join('0') + str).slice(-2)
        }
        return str
      })
      .join('')
      .split('')
      .reverse()
      .join('')
  ).toString('base64')
}

export let uploadFile = async function (file, path) {
  const params = {
    Bucket: myBucket,
    Key: path,
    Body: file,
  }
  await s3
    .upload(params)
    .promise()
    .then((res) => {
      console.log(res)
      // return res.Location
    })
    .catch((err) => {
      console.log(err)
    })
  return this.getSignedUrl(path)
}
