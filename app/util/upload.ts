const multer = require('multer')
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // // cb(null, __basedir + "/resources/static/assets/uploads/");
    // cb(null, "uploads/");

    if(file.fieldname === 'avatar'){
        cb(null, "storage/public/avatars/");
    }else if(file.fieldname === 'universityImage'){
        cb(null, "storage/public/universities/");
    }else if(file.fieldname === 'documentProof'){
        cb(null, "storage/private/documentProofs/")
    }else{
        cb(null, "storage/public/other/");
    }

  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },

});

// const upload = multer({ 
//     storage: storage
// });

const upload = multer({ 
    storage: storage,
    limits: {fileSize: 10000000,},
    fileFilter(req, file, cb) {

        if (!file.originalname.match(/\.(png|jpg|jpeg|bmp|BMP|PNG|JPEG|JPG)$/)){
        cb(new Error('Please upload an image.'))
        }
        

        cb(null, true)
    }
});

module.exports = upload;
