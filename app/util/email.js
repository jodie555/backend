
var nodemailer = require("nodemailer");



module.exports.createTransporter = () => nodemailer.createTransport({
    service: 'yahoo',
    auth: {
        user: process.env.EMAIL_SENDER,
        pass: process.env.EMAIL_SENDER_PWD
    }
});

