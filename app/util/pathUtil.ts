'use strict'

import path from 'path'
export {}

declare global {
  namespace NodeJS {
    interface Global {
      app_path: Function
    }
  }
}

global.app_path = function (file = '/') {
  return path.join(process.cwd(), file)
}
