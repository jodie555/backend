'use strict'

import { Sequelize } from 'sequelize-typescript'

const sequelize = new Sequelize({
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  dialect: 'mysql',
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  storage: ':memory:',
  models: [__dirname + '/models'], // or [Player, Team],
})

export default sequelize
