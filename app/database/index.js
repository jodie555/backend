const Mongoose = require("mongoose");
const Glob = require("glob");
const Path = require("path");
const Sha = require("sha256");


async function firstTimeStartupCheck() {
    let User = Mongoose.model("User");
    let adminUser = await User.find({ email: "systemadmin" });
    if (adminUser.length > 0) {
        console.log("First time startup check completed. Admin user found. Skipping initialization.");
        return true;
    }
    let admin = new User();
    admin.email = "systemadmin";
    admin.password = Sha.x2("joeson!@#");
    admin.name = "System Admin";
    admin.phone = "90001234";
    admin.sysrole = "ADMIN";
    admin.createAt = new Date();
    await admin.save();
  
  
  };



export async function Database(URI) {

    console.log("Connecting to mongodb.");
    // Mongoose.connect(`mongodb://${HOST}:${PORT || 27017}/${DBNAME}`, {
        
    //Mongoose.connect(`mongodb+srv://${USERNAME}:${PASSWORD}@justask.arn3k.mongodb.net/${DBNAME}?retryWrites=true&w=majority`, {
    Mongoose.connect(URI, {
    // Mongoose.connect(`mongodb+srv://${USERNAME}:${PASSWORD}@cluster0.ro1b9.mongodb.net/${DBNAME}?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, (err) => {
        if (err) {
            throw err;
        }
       console.log("Connected to mongodb.");
        //firstTimeStartupCheck();
    });

    // require other schema files 
    let files = Glob.sync('*/database/schema/*.js');
    for (let file of files) {
        console.log(`Requiring schema`, file);
        require(Path.resolve(file));
    }
  }


// module.exports = new Database();




