const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;
const ObjectId = Schema.ObjectId;

const VideoSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    supplierId: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    startAt: Date,
    endAt: Date,
});

var Video

if (Mongoose.models.Video){
    Video = Mongoose.model("Video")
}else{
    Video = Mongoose.model("Video", VideoSchema)
}


// const Video = Mongoose.model("Video", VideoSchema);

module.exports = Video;
