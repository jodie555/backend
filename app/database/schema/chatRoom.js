

const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;
const ObjectId = Schema.ObjectId;

const ChatroomSchema = new Schema({
    // roomName:{
    //     type:String,
    //     required: true,
    // },
    conversationSID:{ //following Twilio naming convention
        type:String,
        // required: true,
    },
    supplierId:{
        type:Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    userId:{
        type:Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    createAt: Date,
    endTime: Date,

});

var Chatroom



if (Mongoose.models.Chatroom){
    Chatroom = Mongoose.model("Chatroom")
}else{
    Chatroom = Mongoose.model("Chatroom", ChatroomSchema)
}

// Chatroom = Mongoose.model("Chatroom", ChatroomSchema)

module.exports = Chatroom;
