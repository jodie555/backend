const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const CourseSchema = new Schema({
    name:{ 
        type:String,
        required: true
    },
    university:{
        type: Schema.Types.ObjectId,
        ref: "University",
        required: true,
    },
    subject:{
        type: Schema.Types.ObjectId,
        ref: "Subject",
        required: true,
    },
});


var Course

if (Mongoose.models.Course){
    Course = Mongoose.model("Course")
}else{
    Course = Mongoose.model("Course", CourseSchema)
}

// const Course = Mongoose.model("Course", CourseSchema);

module.exports = Course;