const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;
const ObjectId = Schema.ObjectId;

const TestSchema = new Schema({
    // roomName:{
    //     type:String,
    //     required: true,
    // },
    conversationSID:{ //following Twilio naming convention
        type:String,
        // required: true,
    },
    imageUrl:{
        type:String,

    },
    createAt: Date,

});

var Test

if (Mongoose.models.Test){
    Test = Mongoose.model("Test")
}else{
    Test = Mongoose.model("Test", TestSchema)
}


// const Test = Mongoose.model("Test", TestSchema);

module.exports = Test;
