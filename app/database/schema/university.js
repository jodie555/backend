const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const UniversitySchema = new Schema({
    name:{ 
        type:String,
        required: true,
        unique:true
    },
    imageURL:{
        type:String
    },
    url:{
        type:String
    },
    description:{
        type:String
    }

});


var University

if (Mongoose.models.University){
    University = Mongoose.model("University")
}else{
    University = Mongoose.model("University", UniversitySchema)
}

// const University = Mongoose.model("University", UniversitySchema);

module.exports = University;