const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const SubjectSchema = new Schema({
    name:{ 
        type:String,
        required: true,
        unique:true
    },
    imageURL:{
        type:String
    },
    description:{
        type:String
    },

});

var Subject

if (Mongoose.models.Subject){
    Subject = Mongoose.model("Subject")
}else{
    Subject = Mongoose.model("Subject", SubjectSchema)
}


// const Subject = Mongoose.model("Subject", SubjectSchema);

module.exports = Subject;