const Mongoose = require ("mongoose");
const Schema = Mongoose.Schema;

const TransactionSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    supplierId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    requestId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Request"
    },
    sessionId: {
        type:String,
        required: true
    },
    paymentIntentId: {
        type:String,
        required: true
    },
    amount: {
        type: Number,
        required: true,
        default: 0
    },
    amount_received: {
        type: Number,
        required: true,
        default: 0
    },
    currency: {
        type:String,
        required: true
    },
    status: {
        type:String,
        required: true,
        enum: ['unpaid', 'success', 'failed']
    }
})


var Transaction

if (Mongoose.models.Transaction){
    Transaction = Mongoose.model("Transaction")
}else{
    Transaction = Mongoose.model("Transaction", TransactionSchema)
}



// const Transaction = Mongoose.model("Transaction", TransactionSchema);

module.exports = Transaction;