const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const UniversityIdentitySchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  university: {
    type: Schema.Types.ObjectId,
    ref: "University",
    required: true,
  },
  course: {
    type: Schema.Types.ObjectId,
    ref: "Course",
    required: true,
  },
  verificationEmail: {
    type: String,
    required: true,
    unique:false,
    match:
      /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  verifiedStatus: {
    type: Boolean,
    required: true,
  },
  documentProofs: {
    type: [{ imageUrl: String, documentType: String }],
    validate: (v) => Array.isArray(v) && v.length > 0,
  },
  // documentProofs: [{ imageUrl: String }],
  entryYear: {
    type: Number,
    required: true,
  },
  confirmationToken: {
    type: String,
    required: false,
    unique: true,
    sparse: true
  },
  confirmationTokenCreatedDate: {
    type: Date,
    required: false,
  },
});


var UniversityIdentity

if (Mongoose.models.UniversityIdentity){
  UniversityIdentity = Mongoose.model("UniversityIdentity")
}else{
  UniversityIdentity = Mongoose.model("UniversityIdentity", UniversityIdentitySchema)
}




// const UniversityIdentity = Mongoose.model(
//   "UniversityIdentity",
//   UniversityIdentitySchema
// );

module.exports = UniversityIdentity;
