const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;
const ObjectId = Schema.ObjectId;

const RequestSchema = new Schema({
    type: {
        type: String,
        enum: ['chat','cv','interview'],
        default: 'chat',
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "User"
    },
    price: {
        type: Number,
        // get: p => (p / 100).toFixed(2),
        // set: p => p * 100,
    },
    supplierId: {
        type: Schema.Types.ObjectId,
        // required: false,
        ref: "User"
    },
    transactionId: {
        type: Schema.Types.ObjectId,
        required: false,
        ref: "Transaction"
    },
    chatroomId: { //ongoing chatroom
        type: Schema.Types.ObjectId,
        ref: "Chatroom"
    },
    description: {
        type: String,
        required: false
    },
    university: { //user's requested university
        type: Schema.Types.ObjectId,
        required: false,
        ref: "University"
    },
    subject: { //user's requested subject
        type: Schema.Types.ObjectId,
        required: false,
        ref: "Subject"
    },
    status: { 
        // 0:New Request(no supplier), 
        // 1:Ongoing(Linked to a supplier, after successful payment),
        // 2:Finished,
        // 3:Ongoing(Need to choose a new supplier, after successful payment),
        type: Number,
        required: true,
        default: 0
    },
    complainStatus: {
        type: Number,
        required: true, //0: healthy, 1:has complaint
        default:0,
    },
    requestCreatedTime: {
        type: Date,
        required: true
    },
    requestMeetingTime: {
        type: Date
    },
    acceptedSuppliers: [
        { type: Schema.Types.ObjectId, ref: "User" }
    ],
    possibleSuppliers: [
        { type: Schema.Types.ObjectId, ref: "User" }
    ]
});

// RequestSchema.path('price').get(function(num) {
//     return ( num / 100).toFixed(2);
// });

// RequestSchema.path('price').set(function(num) {
//     return num * 100;
// });

var Request

if (Mongoose.models.Request){
    Request = Mongoose.model("Request")
}else{
    Request = Mongoose.model("Request", RequestSchema)
}



// const Request = Mongoose.model("Request", RequestSchema);

module.exports = Request;

