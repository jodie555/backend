const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;
const ObjectId = Schema.ObjectId;

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    match:
      /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  verifiedStatus: {
    type: Boolean,
    required: false,
    default: 0,
  },
  confirmationToken: {
    type: String,
    required: false,
    unique: true,
  },
  confirmationTokenCreatedDate: {
    type: Date,
    required: false,
  },
  universityIdentities: [
    { type: Schema.Types.ObjectId, ref: "UniversityIdentity" },
  ],
  username: {
    type: String,
    required: false,
  },
  phone: String,
  avatarUrl: String,
  isSupplier: {
    type: Boolean,
    default: false,
  },
  rating: Number,
  firstName: {
    type: String,
    required: false,
  },
  lastName: {
    type: String,
    required: false,
  },
  price: {
    type: Number,
    // required:function(){
    //     return this.isSupplier;
    // }
    required: false,
  },
  introduction: String,
  //enabled: { type: String, default: "Y" },
  createAt: Date,
});

var User

if (Mongoose.models.User){
  User = Mongoose.model("User")
}else{
  User = Mongoose.model("User", UserSchema)
}



// const User = Mongoose.model("User", UserSchema);

module.exports = User;
