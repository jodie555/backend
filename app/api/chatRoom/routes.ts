import Auth from "../../middlewares/auth";
import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/chatrooms", Controller.createChatroom) // no use
        router.get("/api/chatrooms", Controller.listChartrooms) // no use


        // this should be changed to /api/chatrooms/user/:chatRoomId
        // this route and Controller.findChartroomByChatroomId are also for getting conversationSID
        router.get("/api/chatroom/:chatroomId", Controller.getConversationSID)

        router.delete("/api/chatrooms/:roomName", Controller.deleteChartroom) // no use

        // this should be changed to /api/chatrooms/supplier/:chatRoomId, (previous is /api/chatrooms/:chatroomId)
        router.get("/api/chatrooms/supplier/:chatroomId", Controller.findChartroomByChatroomId)
        
        router.get("/api/chatrooms/conversationSID/:conversationSID", Controller.getChatroomInfo)

        //Dummy function for development
        router.get("/api/chatrooms/development/:conversationSID", Controller.development) // no use

}