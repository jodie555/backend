import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
// const ErrorMessages = require("../../functions/errorMessages")
import { Request, Response } from 'express'



export async function createChatroom(req: Request, res: Response){
  console.log("req.body.roomName",req.body.roomName)
  // if(!req.body||!req.body.roomName||!req.body.conversationSid||
  //     !req.body.supplierId||!req.body.userId){
  //         ErrorMessages.invalidBody()
  // }
  
  const roomName = req.body.roomName
  const conversationSid = req.body.conversationSid
  const userId = req.body.userId
  const supplierId = req.body.supplierId

  console.log("roomName",roomName)

  let User = Mongoose.model("User");
  let Chatroom = Mongoose.model("Chatroom");


  const doesSupplierExist = User.exists({_id:supplierId,isSupplier: true})

  if (!doesSupplierExist){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("invalid supplier id"));
  }

  let newChatroom = new Chatroom()
  newChatroom.roomName = roomName
  newChatroom.conversationSid = conversationSid
  newChatroom.userId = userId
  newChatroom.supplierId = supplierId
  newChatroom = await newChatroom.save();
  res.send(newChatroom)
  
  

}


export async function listChartrooms(req: Request, res: Response){

  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.find()
  res.send(result)
  

}


export async function findChartroom(req: Request, res: Response){
  const roomName = req.params.roomName
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.findOne({roomName:roomName})
  res.send(result)
  

}


export async function getConversationSID(req: Request, res: Response){



  if(!req.params.chatroomId){
    throw new Exception(
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
    ).setDetail(__("invalid chat room id"));
  }


  const userId = req.session.auth._id


  const chatRoomId = req.params.chatroomId
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.findOne({_id:chatRoomId,userId:userId})
  res.send(result.conversationSID)
  

}


export async function deleteChartroom(req: Request, res: Response){
  const roomName = req.params.roomName
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.deleteOne({roomName:roomName})
  console.log(result)
  res.send(result)
  

}


export async function findChartroomByChatroomId(req: Request, res: Response){

  if(!req.params.chatroomId){

    throw new Exception(
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
    ).setDetail(__("invalid chat room id"));
  }


  const supplierId = req.session.auth._id



  let User = Mongoose.model("User");
  const doesSupplierExist = User.exists({_id:supplierId,isSupplier: true})


  if (!doesSupplierExist){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("invalid supplier id"));
  }


  const chatroomId = req.params.chatroomId


  // var ObjectId = require('mongoose').Types.ObjectId; 
  
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.findOne({supplierId:supplierId,_id:chatroomId})
  // const result = await Chatroom.findOne({userId:new ObjectId(userId)})
                              .catch(err=>{
                                throw new Exception(
                                  CONSTANT_ERROR.DB_READ_ERROR.message,
                                  CONSTANT_ERROR.DB_READ_ERROR.code
                                ).setDetail(__(err.message));
                              })

  if(!result){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("invalid chat room"));
  }


  res.send(result);
}


export async function getChatroomInfo(req: Request, res: Response){
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.findOne({conversationSID:req.params.conversationSID})
  // const result = await Chatroom.findOne({userId:new ObjectId(userId)})
                              .catch(err=>{
                                throw new Exception(
                                  CONSTANT_ERROR.DB_READ_ERROR.message,
                                  CONSTANT_ERROR.DB_READ_ERROR.code
                                ).setDetail(__(err.message));
                              });
  res.send(result);
}


export async function development(req: Request, res: Response){
  let Chatroom = Mongoose.model("Chatroom");
  const result = await Chatroom.findOne({conversationSID:req.params.conversationSID})
  // const result = await Chatroom.findOne({userId:new ObjectId(userId)})
                              .catch(err=>{
                                throw new Exception(
                                  CONSTANT_ERROR.DB_READ_ERROR.message,
                                  CONSTANT_ERROR.DB_READ_ERROR.code
                                ).setDetail(__(err.message));
                              }); 
  result.createAt = new Date();
  await result.save();
  res.send(result);
}