
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
var fs = require('fs');
import { Request, Response } from 'express'


export async function getUniversityIdentities(req: Request, res: Response){
  let UniversityIdentity = Mongoose.model("UniversityIdentity")
  let UniversityIdentityResult = await UniversityIdentity.find({})
  res.send(UniversityIdentityResult)
}


export async function getUniversityIdentitiesById(req: Request, res: Response){

  // validation.sessionAuth(req)


  const userId = req.session.auth._id
  // const userId = "60dc85f99ae7d738d97d021d"
  // const userId = "60dc85f99ae7d738d97d022d"

  

  let UniversityIdentity = Mongoose.model("UniversityIdentity")
  let UniversityIdentityResult = await UniversityIdentity.findOne({_id:req.params.id, userId: userId })
  res.send(UniversityIdentityResult)
}


export async function createUniversityIdentities(req: Request, res: Response){


  // validation.sessionAuth(req)


  // const doesUserExist = User.exists({_id:userId})

  // if (!doesUserExist){
  //     const error = new Error('Validation failed, body information is is not valid')
  //     error.status = 422;
  //     error.type= "ValidationFailed"
  //     throw error;
  // }
  
  // if( !req.body
  //     ||!req.body.university
  //     ||!req.body.course
  //     ||!req.body.verificationEmail

  //     ){

  //     const error = new Error('Invalid body')
  //     error.status = 422;
  //     error.type= "ValidationFailed"
  //     throw error;
      
  //   }
  interface MulterRequest extends Request {
    files: any;
  }
  const documentProof = (req as MulterRequest).files['documentProof'][0];
  // const documentProof = req.file;


  console.log("documentProof",documentProof)
  let documentProofUrl = null;
  if (documentProof) {
      documentProofUrl = documentProof.path.replace(/\\/g, "/");
  }

  let UniversityIdentity = Mongoose.model("UniversityIdentity");
  let newUniversityIdentity = new UniversityIdentity();
  // newUniversityIdentity.userId = req.body.userId
  newUniversityIdentity.userId = req.session.auth._id

  newUniversityIdentity.university = req.body.university
  newUniversityIdentity.course = req.body.course
  newUniversityIdentity.verificationEmail = req.body.verificationEmail
  newUniversityIdentity.verifiedStatus = false
  newUniversityIdentity.documentProofs.push({imageUrl:documentProofUrl})
  newUniversityIdentity.entryYear = req.body.entryYear

  
  newUniversityIdentity = await newUniversityIdentity
      .save()
      .catch(err => {
        throw new Exception(
          CONSTANT_ERROR.DB_SAVE_ERROR.message,
          CONSTANT_ERROR.DB_SAVE_ERROR.code
        ).setDetail(__(err.message));
          
      })


  let User = Mongoose.model("User");
  await User.findByIdAndUpdate(
      req.session.auth._id,
      {
          $push: {
              universityIdentities: newUniversityIdentity._id
          }
      },
      { new: true, useFindAndModify: false }
  )

  res.send({
      status: true,
      subjectId:newUniversityIdentity._id,
  })
}


export async function getUniversityIdentitiesDocumentProof(req: Request, res: Response){
  // validation.sessionAuth(req)

  let UniversityIdentity = Mongoose.model("UniversityIdentity");
  const RequestResult = await UniversityIdentity.findOne({"documentProofs._id": req.params.documentProofId})
  const imagePath = RequestResult.documentProofs[0].imageUrl
  

  const data = await fs.readFileSync(imagePath);
  // res.send(data)

  res.type('image').send(data)
}