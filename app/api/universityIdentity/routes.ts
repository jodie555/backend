import * as Controller  from "./controllers"
const upload = require('../../util/upload')

const uploadFields = [
        { name: 'documentProof', maxCount: 1},
        { name: 'avatar', maxCount: 1},
    ]


module.exports = router => {
        router.get("/api/universityIdentities/:id", Controller.getUniversityIdentitiesById); // to do

        router.get("/api/universityIdentities/documentProof/:documentProofId", Controller.getUniversityIdentitiesDocumentProof); // to do
        
        /**
         * need to fix it
         */
        router.post("/api/universityIdentities",upload.fields(uploadFields) , Controller.createUniversityIdentities);

}