const STRIPE_SECRET_KEY = 'sk_test_51HT2lEFLwR7dWnVk9VotBZohcKCqyoawL43AXzrvnO4L3J2bvV7B90WWnTNCPIkrZ4OSek9b8lXFagRFLC0hEhJW00NQVpR0tE';
const stripe = require('stripe')(STRIPE_SECRET_KEY);
const Mongoose = require("mongoose");
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { Request, Response } from 'express'

export async function createCheckoutSession(req: Request, res: Response){
        //TODO read supplier price from database with supplier_id
        let Supplier = Mongoose.model("User")
        const matchedSupplier = await Supplier.findOne({_id: req.body.supplierId}).
                                                populate('university', 'name').
                                                populate('universityMajor', 'name');
        const productName = "Chat with " + matchedSupplier.firstName + " from " 
                            + matchedSupplier.university.name + " studying " + 
                            matchedSupplier.universityMajor.name;
        const productPrice = 200;
        const session = await stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            line_items: [
                {   
                    price_data: {
                        currency: 'usd',
                        product_data: {
                            name: productName,
                        },
                        unit_amount: productPrice,
                    },
                    quantity: 1,
                },
            ],
            mode: 'payment',
            success_url: 'http://localhost:3000/payment-success?id={CHECKOUT_SESSION_ID}',
            cancel_url: 'https://example.com/cancel',
        })

        //Create new transaction in DB with info from session created 
        let newTransactionId;
        let Transaction = Mongoose.model("Transaction")
        let newTransaction = new Transaction()
        newTransaction.userId = req.session.auth._id
        newTransaction.supplierId = req.body.supplierId
        newTransaction.requestId = req.body.requestId
        newTransaction.sessionId = session.id
        newTransaction.paymentIntentId = session.payment_intent
        newTransaction.amount = session.amount_total
        newTransaction.currency = session.currency
        newTransaction.status = session.payment_status
        newTransaction = await newTransaction
            .save()
            .then(result => {
                console.log("Transaction", result)
                newTransactionId = result._id
            })
            .catch(err => {
              throw new Exception(
                CONSTANT_ERROR.DB_SAVE_ERROR.message,
                CONSTANT_ERROR.DB_SAVE_ERROR.code
              ).setDetail(err.message);
            })
        
        //Update the request's transaction field to this new transaction id
        let Request = Mongoose.model("Request");
        const newRequestResult = await Request.findOneAndUpdate({_id: req.body.requestId},
            {transactionId: newTransactionId}, {new: true})

        return res.send({id: session.id});
        
    }

export async function handlePayoutWebhook(req: Request, res: Response){
        let event;
        let Transaction = Mongoose.model("Transaction");

        /**
         * need to modify
         */
        try{
            event = req.body;
        } catch (err) {
          throw new Exception(
            CONSTANT_ERROR.DB_SAVE_ERROR.message,
            CONSTANT_ERROR.DB_SAVE_ERROR.code
          ).setDetail(err.message);
        }

        switch (event.type) {
            case 'payment_intent.succeeded':
                const paymentIntent = event.data.object;
                console.log('Payment Successful!', paymentIntent);

                //Update that Transaction status to success
                if (paymentIntent.amount === paymentIntent.amount_received){

                    const succeededTransaction = await Transaction.findOneAndUpdate({paymentIntentId: paymentIntent.id},
                        {status: "success", amount_received: paymentIntent.amount_received},
                        {new: true})
                    console.log("Updated Transaction Result: ", succeededTransaction)
                    
                    if (succeededTransaction.status === "success"){
                        //Update that Transaction Request's status from 1 to 2 and Request's supplier
                        let Request = Mongoose.model("Request");
                        const updatedRequestResult = await Request.findOneAndUpdate({_id: succeededTransaction.requestId},
                            {status: 2, supplierId: succeededTransaction.supplierId}, {new: true})
                        console.log("New Request Result", updatedRequestResult)
                    }


                } else {
                    const succeededTransaction = await Transaction.findOneAndUpdate({paymentIntentId: paymentIntent.id},
                        {amount_received: paymentIntent.amount_received},
                        {new: true})
                    console.log("Updated Transaction Result: ", succeededTransaction)
                }

            case 'payment_intent.payment_failed':
                //Update that Transaction status to failed
                console.log("Payment failed!")
                const failedTransaction = await Transaction.findOneAndUpdate({paymentIntentId: paymentIntent},
                    {status: "failed", amount_received: paymentIntent.amount_received},
                    {new: true})
                console.log("Updated Transaction Result: ", failedTransaction)

                //TODO notify user that the transaction failed.

            case 'payment_method.attached':
                const paymentMethod = event.data.object;
                console.log('payment intent attached');
                console.log(paymentMethod);
            default:
                console.log(`Unhandled event type ${event.type}`);
        }

        return res.send({received: true});
    }
