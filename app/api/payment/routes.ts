import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/payment/checkout_session", Controller.createCheckoutSession);
        router.post("/api/payment/webhook", Controller.handlePayoutWebhook);

}