
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");

import { Request, Response } from 'express'


const AccessToken = require('twilio').jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;
const ChatGrant = AccessToken.ChatGrant;


const twilioAccountSid = process.env.TWILIO_ACCOUNT_SID;
const twilioAuthToken = process.env.TWILIO_AUTH_TOKEN;
const twilioApiSecret = process.env.TWILIO_API_SECRET;
const twilioApiKey = process.env.TWILIO_API_KEY;

let Video = Mongoose.model("Video");
const io = require('../../socket');


export async function createVideoCall(req: Request, res: Response){
  let videoResult = await Video.findOne({userId:req.session.auth._id, supplierId:req.body.supplierId}).exec();
  console.log("userId",req.session.auth._id)
  console.log("supplierId",req.body.supplierId)

  const userId = req.session.auth._id.toString()
  let videoId;
  if (videoResult){
      videoId = videoResult._id;
  } else {
      let newVideo = new Video()
      newVideo.userId = req.session.auth._id
      newVideo.supplierId = req.body.supplierId
      newVideo.startAt = new Date() // the starting time of the video call
      newVideo = await newVideo
          .save()
          .catch(error => {
              if(!error.status){
              error.status = 500
              }
              throw error     
          })
      videoId = newVideo._id;
  }

  const accountSid = process.env.TWILIO_ACCOUNT_SID;
  const authToken = process.env.TWILIO_AUTH_TOKEN;
  const conversation_service_sid = process.env.TWILIO_CHAT_SERVICE_SID


  const client = require('twilio')(accountSid, authToken);
  const conversationsClient = client.conversations.services(conversation_service_sid);

  const videoStringId = videoId.toString()
  // const videoStringId = "abcde"

  try {
    // See if conversation already exists
    await conversationsClient.conversations(videoStringId).fetch();
  } catch (e) {
    try {
      // If conversation doesn't exist, create it.
      // Here we add a timer to close the conversation after the maximum length of a room (24 hours).
      // This helps to clean up old conversations since there is a limit that a single participant
      // can not be added to more than 1,000 open conversations.
      await conversationsClient.conversations.create({ uniqueName: videoStringId, 'timers.closed': 'P1D' });
    } catch (e) {

    }
  }

  try {
    // Add participant to conversation
    await conversationsClient.conversations(videoStringId).participants.create({ identity: userId });
    await conversationsClient.conversations(videoStringId).participants.create({ identity: req.body.supplierId });

  } catch (e) {

  }


  const videoGrant = new VideoGrant({
      room: videoId
  })
  const chatGrant = new ChatGrant({
    serviceSid: conversation_service_sid,
  });
  const token = new AccessToken(
      twilioAccountSid,
      twilioApiKey,
      twilioApiSecret,
      {identity: userId}
  );
  
  token.addGrant(videoGrant);
  token.addGrant(chatGrant);

  io.getIO().emit('6227eef80126d41601c2b9d5', {
    action: 'create',
    post: "dd"
  });

  
  res.send({token: token.toJwt()});
}

//To be called from the supplier side
export async function joinVideoCall(req: Request, res: Response){
  const conversation_service_sid = process.env.TWILIO_CHAT_SERVICE_SID
  const supplierId = req.session.auth._id.toString()

  let videoResult = await Video.findOne({userId:req.body.userId, supplierId:supplierId}).exec();

  let videoId;
  if (videoResult){
      videoId = videoResult._id;

      const videoGrant = new VideoGrant({
          room: videoId
      })
      const chatGrant = new ChatGrant({
        serviceSid: conversation_service_sid,
      });

      const token = new AccessToken(
          twilioAccountSid,
          twilioApiKey,
          twilioApiSecret,
          {identity: supplierId}
      );

      token.addGrant(videoGrant);
      token.addGrant(chatGrant);


      return res.send({token: token.toJwt()});

  } else {
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("invalid video token"));
  }

}

//to be called by user
export async function endVideoCall(req: Request, res: Response){
    try{
        let videoResult = await Video.findOne({userId:req.session.auth._id, supplierId:req.body.supplierId}).exec();

        let endTime = new Date();
        videoResult.updateOne({userId:req.session.auth._id, supplierId:req.body.supplierId}, 
            { $set: { endAt: endTime} 
            }
        )
    } catch (error){
        if(!error.status){
            error.status = 500
          }
        throw error
    }
}




export async function  getVideoCall(req: Request, res: Response){
  try{
      console.log(req.params.requestId)
      let Request = Mongoose.model("Request");
      let requestResult = await Request.findOne({_id:req.params.requestId}).exec();
      console.log("1", requestResult)
      let videoResult = await Video.findOne({userId: requestResult.userId, supplierId: requestResult.supplierId}).exec();
      console.log("2", requestResult)
      console.log("3", videoResult)
      
      res.send(videoResult)
  } catch (error){
      if(!error.status){
          error.status = 500
        }
      throw error
  }
}