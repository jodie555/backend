import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/create_video", Controller.createVideoCall)
        router.post("/api/join_video", Controller.joinVideoCall)        //To be called by supplier
        router.post("/api/end_call", Controller.endVideoCall)           //To be called by user
        router.get("/api/getVideoCall/:requestId", Controller.getVideoCall)
}