import * as Controller  from "./controllers"

module.exports = router => {
        router.get("/api/subjects", Controller.getSubjects);
        router.post("/api/subjects", Controller.createNewSubject);

}