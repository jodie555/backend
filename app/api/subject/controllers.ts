import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");

import { Request, Response } from 'express'




export async function getSubjects(req: Request, res: Response){
    let Subject = Mongoose.model("Subject")
    let subjectsResult = await Subject.find({}).select('name')
    res.send(subjectsResult)
}

export async function createNewSubject(req: Request, res: Response){
    
    // if( !req.body
    //     ||!req.body.name){

    //     ErrorMessages.invalidBody()
        
    //   }

    const subjectName = req.body.name
    const subjectImageURL = req.body.imageURL
    const subjectDescription = req.body.description



    let Subject = Mongoose.model("Subject");
    
    //   check whether the input body is valid
    const doesSubjectExist = await Subject.exists({name:subjectName})
    console.log(doesSubjectExist)

    if (doesSubjectExist){
      throw new Exception(
        CONSTANT_ERROR.DB_READ_ERROR.message,
        CONSTANT_ERROR.DB_READ_ERROR.code
      ).setDetail(__("document not exist"));
    }

    let newSubject = new Subject();
    newSubject.name = subjectName
    newSubject.imageURL = subjectImageURL
    newSubject.description = subjectDescription
    
    newSubject = await newSubject
        .save()
        .catch(err => {
          throw new Exception(
            CONSTANT_ERROR.DB_SAVE_ERROR.message,
            CONSTANT_ERROR.DB_SAVE_ERROR.code
          ).setDetail(__(err.message));
            
        })

    res.send({
        status: true,
        subjectId:newSubject._id,
    })
}