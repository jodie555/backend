import Auth from "../../middlewares/auth";
import * as Controller  from "./controllers"

module.exports = router => {
        //this function should also create and save the new chat room to the database directly
        router.post("/api/create_conversation/by_body", Controller.createConversationRoomByBody)


        router.post("/api/create_conversation/by_session", Controller.createConversationRoomBySession)


        // router.post("/api/create_conversation/:supplierId", Controller.createConversationRoomBySupplierId)  //no use
        


        
        router.post("/api/create_conversation/request", Controller.createConversationRoomRequest)

        
        router.get("/api/get_conversationId/:chatroomId", Controller.getConversationId)


        // get the room name stored in session
        router.get("/api/session/roomName", Controller.getSessionRoomName)

        // upload a body with roomName and store the roomName into session
        router.post("/api/session/roomName", Controller.createRoom)


        router.get("/api/conversation_service_sid/:sid", Controller.getConversationBySid)

        // router.post("/api/add_participant/body_identity", Controller.addParticipantByBody)


        router.post("/api/add_participant/session_identity", Controller.addParticipantByBodyAndSession)


        router.delete("/api/remove_conversation", Controller.removeConversationRoom)


        router.get("/api/test",Controller.test)

        router.get("/api/read_service",Controller.getMultiConversations)


        // fastify.get('/token/:id', (req, res) => {
        //   const id = req.params.id;
        //   res.send(tokenGenerator(id));
        // });

        router.get('/token/email', Controller.getEmailToken);


        router.get('/token/id',[Auth.api], Controller.getIdToken);

}