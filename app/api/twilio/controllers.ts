import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
const tokenGenerator = require('./token_generator');
const config = require('./config');
const camelCase = require('camelcase');
function camelCaseKeys(hashmap) {
    var newhashmap = {};
    Object.keys(hashmap).forEach(function(key) {
      var newkey = camelCase(key);
      newhashmap[newkey] = hashmap[key];
    });
    return newhashmap;
  };
const axios = require("axios");
import { Request, Response } from 'express'

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;


export async function createConversationRoomByBody(req: Request, res: Response){
  // if(!req.body||!req.body.uniqueName){

  //     ErrorMessages.invalidBody()

  //   }
  const client = require('twilio')(accountSid, authToken);
    
  client.conversations.conversations
                      .create({uniqueName: req.body.uniqueName})
                      .then(conversation => {

                          
                          console.log(conversation.sid)
                          res.send({conversation:conversation})

                      });

}   

export async function createConversationRoomBySession(req: Request, res: Response){
  if(req.session.roomName == undefined){

    throw new Exception(
      CONSTANT_ERROR.INVALID_SESSION.message,
      CONSTANT_ERROR.INVALID_SESSION.code
    ).setDetail(__("password not match"));
  }

  const client = require('twilio')(accountSid, authToken);
  console.log('req.session.roomName',req.session.roomName)
  client.conversations.conversations
                      .create({uniqueName: req.session.roomName})
                      .then(conversation => {

                          
                          console.log("conversation.sid",conversation.sid)
                          res.send({conversation:conversation})
                        
                      });

}


export async function createConversationRoomBySupplierId(req: Request, res: Response){

    // Validation.sessionAuth(req)


    if(!req.params.supplierId){

      throw new Exception(
        CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
        CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
      )    
    }

    const supplierId = req.params.supplierId

    
    let User = Mongoose.model("User");
    // check whether the input supplier id is a valid supplier id
    const result = await User.exists({_id:supplierId,isSupplier: true})
    console.log("result supplier",result)


    if(!result || result == []){
      throw new Exception(
        CONSTANT_ERROR.DB_READ_ERROR.message,
        CONSTANT_ERROR.DB_READ_ERROR.code
      ).setDetail(__("cannot find supplier"));

    }

    // check whether they have created a chat room before
    let Chatroom = Mongoose.model("Chatroom");
    let ChatroomExist: any = {};
    // let ChatroomExist = false

    try{
      ChatroomExist = await Chatroom.findOne({supplierId:supplierId,userId: req.session.auth._id}).exec()
    } catch (err){
      throw new Exception(
        CONSTANT_ERROR.DB_READ_ERROR.message,
        CONSTANT_ERROR.DB_READ_ERROR.code
      ).setDetail(__(err.message));
      
    }

    console.log("ChatroomExist",ChatroomExist)
    if(ChatroomExist){
      // if we find one , we will send back the corresponding conversation sid 
      return res.send({conversationSID:ChatroomExist.conversationSID})
    }


    let newChatroom = new Chatroom()
    newChatroom.userId = req.session.auth._id
    newChatroom.supplierId = supplierId
    newChatroom.createAt = new Date()
    newChatroom = await newChatroom.save();

    const client = require('twilio')(accountSid, authToken);  
    const newChatroomId = newChatroom._id.toString()


    // try to create a new conversation room by using the new roomName
    const conversation = await client.conversations.conversations
    .create({uniqueName: newChatroomId})
    .then(async conversation => {
      newChatroom.conversationSID = conversation.sid
      newChatroom = await newChatroom.save();
      return conversation

    }).catch(async err=>{
      throw new Exception(
        CONSTANT_ERROR.TWILIO_ERROR.message,
        CONSTANT_ERROR.TWILIO_ERROR.code
      ).setDetail(__(err.message));
      
    })


    // add supplier to the conversation room 
    await client.conversations.conversations(conversation.sid)
    .participants
    .create({identity: supplierId})
    .catch(err=>{
      throw new Exception(
        CONSTANT_ERROR.TWILIO_ERROR.message,
        CONSTANT_ERROR.TWILIO_ERROR.code
      ).setDetail(__(err.message));
    })

    // add user to the conversation room 
    await client.conversations.conversations(conversation.sid)
    .participants
    .create({identity: req.session.auth._id.toString()})
    .catch(err=>{
      throw new Exception(
        CONSTANT_ERROR.TWILIO_ERROR.message,
        CONSTANT_ERROR.TWILIO_ERROR.code
      ).setDetail(__(err.message));
    })

    res.send({conversationSID:conversation.sid})
}


export async function createConversationRoomRequest(req: Request, res: Response){

  // if( !req.body
  //   ||!req.body.requestId){

  //   ErrorMessages.invalidBody()
    
  // }



  const requestId = req.body.requestId
  const userId = req.session.auth._id


  // check whether the request have a chat room or not
  // and also whether the user is the real user of this request
  let Request = Mongoose.model("Request");
  let ChatroomCreated = false

  let requestInfo = null

  try{
    requestInfo = await Request.findOne({_id:requestId,userId:userId}).exec()
  } catch (err){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__(err.message));
    
  }
  

  if(!requestInfo){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("cannot_find_request"));

  }

  if(!requestInfo.supplierId){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("no_supplier_in_request"));
  }



//if the request already has a chat room, we will return the sid to front end
  let Chatroom = Mongoose.model("Chatroom");
  let ChatroomExist: any = {};


  if(requestInfo.chatroomId){
    try{
      ChatroomExist = await Chatroom.findOne({_id:requestInfo.chatroomId}).exec()
      return res.send({conversationSID:ChatroomExist.conversationSID})
    } catch (err){
      throw new Exception(
        CONSTANT_ERROR.DB_READ_ERROR.message,
        CONSTANT_ERROR.DB_READ_ERROR.code
      ).setDetail(__(err.message));
      
    }
  }


  const supplierId = requestInfo.supplierId.toString()

  let newChatroom = new Chatroom()
  newChatroom.userId = userId
  newChatroom.supplierId = supplierId
  newChatroom.createAt = new Date()
  newChatroom = await newChatroom.save();

  const client = require('twilio')(accountSid, authToken);  
  const newChatroomId = newChatroom._id.toString()


  // try to create a new conversation room by using the new roomName
  const conversation = await client.conversations.conversations
  .create({uniqueName: newChatroomId})
  .then(async conversation => {
    newChatroom.conversationSID = conversation.sid
    newChatroom = await newChatroom.save();
    requestInfo.chatroomId = newChatroom._id
    await requestInfo.save()
    return conversation

  }).catch(async err=>{
    throw new Exception(
      CONSTANT_ERROR.TWILIO_ERROR.message,
      CONSTANT_ERROR.TWILIO_ERROR.code
    ).setDetail(__(err.message));
    
  })





  // add supplier to the conversation room 
  await client.conversations.conversations(conversation.sid)
  .participants
  .create({identity: supplierId})
  .catch(err=>{
    throw new Exception(
      CONSTANT_ERROR.TWILIO_ERROR.message,
      CONSTANT_ERROR.TWILIO_ERROR.code
    ).setDetail(__(err.message));
  })

  // add user to the conversation room 
  await client.conversations.conversations(conversation.sid)
  .participants
  .create({identity: userId.toString()})
  .catch(err=>{
    throw new Exception(
      CONSTANT_ERROR.TWILIO_ERROR.message,
      CONSTANT_ERROR.TWILIO_ERROR.code
    ).setDetail(__(err.message));
  })

  res.send({conversationSID:conversation.sid})

}


export async function getConversationId(req: Request, res: Response){
  let Chatroom = Mongoose.model("Chatroom");
  console.log(req.params.chatroomId)
  let result = await Chatroom.findOne({_id:req.params.chatroomId}).exec()

  if(result){
    return res.send({conversationSID: result.conversationSID})
  }else{
    return res.send({})
  }
  
}



export async function  getSessionRoomName(req: Request, res: Response){
  try{
      if(req.session.roomName == undefined ){

        throw new Exception(
          CONSTANT_ERROR.INVALID_SESSION.message,
          CONSTANT_ERROR.INVALID_SESSION.code
        )
      }
      else{
        
        res.send({roomName:req.session.roomName})
      }
    } catch(err){
      throw new Exception(
        CONSTANT_ERROR.INVALID_SESSION.message,
        CONSTANT_ERROR.INVALID_SESSION.code
      ).setDetail(__(err.message))
    }
}


export async function createRoom(req: Request, res: Response){
  // if(!req.body||!req.body.roomName){

  //     ErrorMessages.invalidBody()
  //   }

  req.session.roomName = req.body.roomName
  res.send({roomName : req.session.roomName})

    
}


export async function getConversationBySid(req: Request, res: Response){
  if(!req.params.sid){
    throw new Exception(
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
    ).setDetail(__("no sid"))
  }
  const id = req.params.sid;
  console.log("id",id)
  const client = require('twilio')(accountSid, authToken);
  
  client.conversations.conversations(id)
  .fetch()
  .then(conversation => {
      console.log(conversation.chatServiceSid)
      res.send({conversation:conversation})
  });
    
}



export async function functionaddParticipantByBody(req: Request, res: Response){
  // if(!req.body||!req.body.conversationSID||!req.body.identity){

  //     ErrorMessages.invalidBody()
  //   } 

  const client = require('twilio')(accountSid, authToken);
  client.conversations.conversations(req.body.conversationSID)
                      .participants
                      .create({identity: req.body.identity})
                      .then(participant => {
                        
                          console.log(participant.sid)
                          return res.send(participant)
                        });

    
}


export async function addParticipantByBodyAndSession(req: Request, res: Response){

  // Validation.sessionAuth(req)


  // if(!req.body || !req.body.conversationSID){

  //     ErrorMessages.invalidBody()

  //   }
  console.log("identity",req.session.auth.email)
  const client = require('twilio')(accountSid, authToken);
  client.conversations.conversations(req.body.conversationSID)
                      .participants
                      .create({identity: req.session.auth.email})
                      .then(participant => {
                        
                          console.log(participant.sid)
                          return res.send(participant)
                        });

    
}


export async function removeConversationRoom(req: Request, res: Response){

  // if(!req.body||!req.body.conversationSID){
  //     ErrorMessages.invalidBody()

  //   }

  const client = require('twilio')(accountSid, authToken);
  client.conversations.conversations(req.body.conversationSID)
                      .remove()
                      .then(()=> res.send({message:"Finished"}))


}


export async function test(req: Request, res: Response){
    // const uniqueName = req.params.uniqueName
    const client = require('twilio')(accountSid, authToken);
    client.conversations.conversations.list((result)=>{return res.send(result)})
}

export async function getMultiConversations(req: Request, res: Response){

  const client = require('twilio')(accountSid, authToken);
  client.conversations.services
  .list({limit: 20})
  .then(services => services.forEach(s => {
    if(process.env.TWILIO_CHAT_SERVICE_SID == s.sid){
      console.log(s.links.conversations)
      axios.get(s.links.conversations,{
        
          auth:{
            username:accountSid,
            password:authToken
          }

      }).then( result => {
        console.log(result)
      })
      res.send(s.links)
    }

  }));
}


export async function getEmailToken(req: Request, res: Response){

  // if(req.session.auth !== undefined){

  res.send(tokenGenerator(req.session.auth.email));

}

export async function getIdToken(req, res){

  // Validation.sessionAuth(req)

  res.send(tokenGenerator(req.session.auth._id.toString()));

}