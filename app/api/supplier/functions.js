const {createTransporter} = require('../../util/email')
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";


module.exports.sendValidationEmail = (email,token) => {


    const transporter = createTransporter()

    var mailOptions = {
        from: process.env.EMAIL_SENDER,
        to: 'aps19isec@gmail.com',
        subject: 'Email Confirmation',
        html: `<h1>Email Confirmation</h1>
        <h2>Hello New User</h2>
        <p>Thank you for subscribing. Please confirm your email by clicking on the following link</p>
        <a href=http://localhost:3000/user/verification?email=${email}&token=${token}> Click here</a>
        </div>`,
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (err) {
            throw new Exception(
                CONSTANT_ERROR.EMAIL_ERROR.message,
                CONSTANT_ERROR.EMAIL_ERROR.code
              ).setDetail(__(err.message));
        } else {
            console.log('Email sent: ' + info.response);
        }

    });
}


module.exports.sendValidationUniversityEmail = (universityEmail,supplierToken) => {


    const transporter = createTransporter()

    var mailOptions = {
        from: process.env.EMAIL_SENDER,
        to: 'aps19isec@gmail.com',
        subject: 'University Email Confirmation',
        html: `<h1>Email Confirmation</h1>
        <h2>Hello New User</h2>
        <p>Thank you for subscribing. Please confirm your email by clicking on the following link</p>
        <a href=http://localhost:3000/supplier/verification?universityEmail=${universityEmail}&supplierToken=${supplierToken}> Click here</a>
        </div>`,
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (err) {
            throw new Exception(
                CONSTANT_ERROR.EMAIL_ERROR.message,
                CONSTANT_ERROR.EMAIL_ERROR.code
              ).setDetail(__(err.message));
        } else {
            console.log('Email sent: ' + info.response);
        }

    });
}

