import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";
const {sendValidationEmail,sendValidationUniversityEmail} = require("./functions");
const Mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");

import { Request, Response } from 'express'

interface MulterRequest extends Request {
  files: any;
}



export async function createSupplier(req: Request, res: Response){

  // if( !req.body
  //     ||!req.body.email
  //     ||!req.body.password
  //     // ||!req.body.username
  //     ||!req.body.university
  //     ||!req.body.universityEmail
  //     ||!req.body.universityMajor


  //   ){

  //     ErrorMessages.invalidBody()
      
  // }
  const avatar = (req as MulterRequest).files['avatar'][0]
  let avatarUrl = null
  if(avatar){
      console.log("avatar.path",avatar.path)
      avatarUrl = avatar.path.replace(/\\/g, "/")
  }
  console.log("email",req.body.email)
  let User = Mongoose.model("User");
  let isUserExist = await User.exists({email: req.body.email})
  if (isUserExist) {
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("user is already exist"));
  }

  const hash = await bcrypt.hash(req.body.password, 10)
                  .catch(err => {
                    throw new Exception(
                      CONSTANT_ERROR.HASH_ERROR.message,
                      CONSTANT_ERROR.HASH_ERROR.code
                    ).setDetail(__(err.message));
                  })

  const email = req.body.email
  const universityEmail = req.body.universityEmail
  // const username = req.body.username


  const userToken = jwt.sign({email: email}, process.env.JWT_SECRET)
  const supplierToken = jwt.sign({universityEmail: universityEmail}, process.env.JWT_SECRET)
  

  sendValidationEmail(email,userToken)
  // sendVerificationUniversityEmail(username,universityEmail,supplierToken)
  
  let newUser = new User()
  newUser.email = email
  newUser.password = hash
  // newUser.username = username
  newUser.university = req.body.university
  newUser.universityEmail = universityEmail
  newUser.universityMajor = req.body.universityMajor
  newUser.confirmationToken = userToken
  newUser.isSupplier = true
  newUser.avatarUrl = avatarUrl
  newUser.createAt = new Date()

  await newUser.save().catch(err => {
    throw new Exception(
      CONSTANT_ERROR.DB_SAVE_ERROR.message,
      CONSTANT_ERROR.DB_SAVE_ERROR.code
    ).setDetail(__(err.message));
  })
  

  res.send({
      status: true,
      data: {},
  })
  
}


export async function sendValidationToUniversityEmail(req: Request, res: Response){

  // if( !req.body
  //     ||!req.body._id
  //   ){

  //     ErrorMessages.invalidBody()
      
  // }


  let User = Mongoose.model("User")
  const user = await User.findOne({_id : Mongoose.Types.ObjectId(req.body._id),verifiedSupplierStatus:{$ne: true}})
  if(!user){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("user is not exist"));
  }

  const universityEmail = user.universityEmail
  // const username = user.username

  const supplierToken = jwt.sign({universityEmail: universityEmail}, process.env.JWT_SECRET)

  sendValidationUniversityEmail(universityEmail,supplierToken)

  res.send({
      status:true,
      message: "success"
  })
}

// async createSupplier(req, res){
//     let SupplierModel = Mongoose.model("User");
//     let isSupplierExist = await SupplierModel.find({email: req.body.email})
//     if (!isSupplierExist) {
//         res.status(422);
//         res.send({
//             status: false,
//             error: err,
//             code: err.code,
//         });
//         return
//     }

//     bcrypt.hash(req.body.password, 10, async (err, hash) => {
//         if(err){
//             res.status(500);
//             res.send({
//                 status: false,
//                 error: err,
//                 code: err.code,
//             });
//         }else{
//             const supplier = new SupplierModel({
//                 email: req.body.email,
//                 password: hash,
//                 username: req.body.username,
//                 firstName: req.body.firstName,
//                 university: req.body.university,
//                 universityMajor: req.body.universityMajor,
//                 universityEmail: req.body.universityEmail,
//                 createAt: new Date(),
//                 isSupplier: true,
//             })
//             let newSupplier = await supplier.save().catch(err => {
//                 res.status(400);
//                 console.log(err);
//                 res.send({
//                     status: false,
//                     error: err,
//                     code: err.code,
//                 });
//                 return
//             });
//             console.log(newSupplier)
//             sendVerificationEmail()
//             res.send({
//                 status: true,
//                 data: {},
//             });
//         }
//     });
// }

export async function getSuppliers(req: Request, res: Response){
  let User = Mongoose.model("User");
  const result = await User.find({isSupplier: true})
                          .populate({
                              path: 'universityIdentities',
                              select: 'university course',
                              populate: {                        
                                  path: 'university course',
                                  select: 'name name'                       
                              }
                          });

  res.send(result);
}   

export async function getSupplierById(req: Request, res: Response){
  const supplierId = req.params.supplierId
  let User = Mongoose.model("User");
  const result = await User.findOne({_id:supplierId})
                      .populate({
                          path: 'universityIdentities',
                          select: 'university course',
                          populate: {
                              path: 'university course',
                              select: 'name name'
                          }
                      });
  res.send(result)
}