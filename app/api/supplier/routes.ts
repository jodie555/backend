import * as Controller  from "./controllers"
// const multer  = require('multer')
// const upload = multer({ dest: 'uploads/' })
const upload = require('../../util/upload')


const uploadFields = [
        { name: 'documentProof', maxCount: 1},
        { name: 'avatar', maxCount: 1},
    ]


module.exports = router => {
        router.post("/api/suppliers",upload.fields(uploadFields), Controller.createSupplier);
        router.get("/api/suppliers", Controller.getSuppliers);
        router.get("/api/suppliers/:supplierId", Controller.getSupplierById);
        router.post("/api/suppliers/send-validation-to-university-email", Controller.sendValidationToUniversityEmail); //no use
    

}