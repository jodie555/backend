
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
const Sha = require("sha256");
const bcrypt = require("bcrypt");
import { Request, Response } from 'express'

export async function handleLogin(req: Request, res: Response) {
  let { username, password } = req.body;
  let UserModel = Mongoose.model("User");
  let user = await UserModel.findOne({
    email: username,
    password: Sha.x2(password),
  });
  if (!user) {
    throw new Exception(
      CONSTANT_ERROR.UNAUTHENTICATED.message,
      CONSTANT_ERROR.UNAUTHENTICATED.code
    ).setDetail(__("invalid login"));
  }

  req.session.auth = user;

  let result = {
    _id: user._id,
    isSupplier: user.isSupplier,
    verifiedSupplierStatus: user.verifiedSupplierStatus,
  };

  return res.send({
    status: true,
    data: result,
  });
}

export async function handleGetAuth(req: Request, res: Response) {
  // Validation.sessionAuth(req)
  let data = JSON.parse(JSON.stringify(req.session.auth));
  delete data.password;
  return res.send({
    status: true,
    data: data,
  });
}

export async function handleLogout(req: Request, res: Response) {
  req.session.auth = null;
  res.redirect("/");
}

export async function createUser(req: Request, res: Response) {
  // if (!req.body || !req.body.email || !req.body.password) {
  //   ErrorMessages.invalidBody()
  // }

  let User = Mongoose.model("User");
  let isUserExist = await User.exists({ email: req.body.email });
  if (isUserExist) {
    throw new Exception(
      CONSTANT_ERROR.USER_EXIST.message,
      CONSTANT_ERROR.USER_EXIST.code
    )
  }

  const hash = await bcrypt.hash(req.body.password, 10)
  .catch((err) => {
    throw new Exception(
      CONSTANT_ERROR.HASH_ERROR.message,
      CONSTANT_ERROR.HASH_ERROR.code
    ).setDetail(err.message)
  });

  let newUser = new User();
  newUser.email = req.body.email;
  newUser.password = hash;
  // newUser.username = req.body.username
  newUser.isSupplier = false;
  newUser.registerFinished = true;
  newUser.createAt = new Date();
  await newUser.save()
  .catch((err) => {
    throw new Exception(
      CONSTANT_ERROR.DB_SAVE_ERROR.message,
      CONSTANT_ERROR.DB_SAVE_ERROR.code
    ).setDetail(err.message)
  });

  res.send({
    status: true,
    data: {},
  });
}

export async function test(req: Request, res: Response) {
  console.log("checked");
  res.send({
    status: true,
    data: {},
  });
}


export async function getUserId(req: Request, res: Response) {
  const userId = req.session.auth._id

  res.send({
    status: true,
    userId: userId,
  });
}


export async function getUserInfoById(req: Request, res: Response) {
  let User = Mongoose.model("User")

  let UserInfo = await User.findOne({_id:req.params.id})
  console.log("UserInfo",UserInfo)

  const userData = {
    userId:UserInfo._id,
    // firstName:UserInfo.firstName,
    username:UserInfo.username
  }

  res.send({
    status: true,
    data: userData,
  });
}