import * as Controller  from "./controllers"

module.exports = router => {
        // router.post("/api/login", Controller.handleLogin);
        //router.get("/api/auth", Controller.handleGetAuth);
        router.post("/api/users", Controller.createUser);
        router.get("/api/user/id", Controller.getUserId);
        router.get("/api/user/:id", Controller.getUserInfoById);

}