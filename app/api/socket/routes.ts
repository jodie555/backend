import * as Controller  from "./controllers"

module.exports = router => {
        router.get("/api/socket-test", Controller.sendSocket);
        router.post("/api/socket/created-room",Controller.createdRoom)


}