
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";


// const ErrorMessages = require("../../functions/errorMessages")
import { Request, Response } from 'express'



export async function logout(req: Request, res: Response){

  if (req.session.auth){

      req.session.destroy( err=>{

        if(err){
          throw new Exception(
            CONSTANT_ERROR.NOT_LOG_IN.message,
            CONSTANT_ERROR.NOT_LOG_IN.code
          ).setDetail(err);
        }else{
          res.send({
            status: true,
            data: {
                message: "Logged out successfully"
            },
          })
        }
      })

      


  }else{
    throw new Exception(
      CONSTANT_ERROR.NOT_LOG_IN.message,
      CONSTANT_ERROR.NOT_LOG_IN.code
    ).setDetail("invalid logout");
  }

  

}