import * as Controller  from "./controllers"

module.exports = router => {
        router.delete("/api/logout", Controller.logout)

}