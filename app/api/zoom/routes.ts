import * as Controller  from "./controllers"

module.exports = router => {
        router.get("/api/zoom/getCode", Controller.zoomGetCode)
        router.get("/api/zoom/getRoomUrl", Controller.zoomGetRoomUrl)
        router.get("/api/zoom/createUser", Controller.zoomCreateUser)
        router.get("/api/zoom/listUsers", Controller.zoomListUsers)
        router.get("/api/zoom/createRoom", Controller.zoomCreateUserSuccess)
        router.get("/api/zoom/addUser", Controller.zoomAddUser)
        router.get("/api/zoom/userList", Controller.zoomUserList)
        router.get("/api/zoom/createRoomByMain", Controller.zoomMainUserCreateRoom)
        router.get("/api/zoom/deleteUser", Controller.zoomDeleteUser)
}