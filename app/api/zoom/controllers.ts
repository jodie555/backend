
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");

import { Request, Response } from 'express'
const axios = require("axios")
const request = require('request');



export async function zoomGetCode(req: Request, res: Response) {
  if (req.query.code) {

      // Step 3: 
      // Request an access token using the auth code

      let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;
      let base64 = new Buffer(process.env.ZOOM_CLIENT_ID+':'+process.env.ZOOM_CLIENT_SECRET).toString('base64')

      axios.post(url,{}, {
          headers: {
              'Authorization': 'Basic '+base64,
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Methods": "POST"

          }
      })
      .then( (response) => {
          // Parse response to JSON
          // body = JSON.parse(body);
          // console.log("response",response.data)
          var body = response.data

          // Logs your access and refresh tokens in the browser
          console.log(`access_token: ${body.access_token}`);
          console.log(`refresh_token: ${body.refresh_token}`);

          if (body!=null){
              req.session.zoom_auth = body;
              res.send("success")

          }else{
              res.send("fail")
          }

          

          // if (body.access_token) {
              
          //     const config = {
                  
          //         headers: {'authorization': 'Bearer '+body.access_token}
          //     };

          //     // Step 4:
          //     // We can now use the access token to authenticate API calls

          //     // Send a request to get your user information using the /me context
          //     // The `/me` context restricts an API call to the user the token belongs to
          //     // This helps make calls to user-specific endpoints instead of storing the userID
          //     console.log("hello I am here")
              
          //     axios.get('https://api.zoom.us/v2/users/me',config).then(

          //     (response) => {
          //             const body = response.data
          //             // body = JSON.parse(body.data);
          //             // Display response in console
          //             console.log('API call ', body);
          //             // Display response in browser
          //             res.send({status: true});
                  
          //     }).catch((error)=>{
          //         console.log("error",error)
          //     })

          // } else {
          //     // Handle errors, something's gone wrong!
          // }

      }).catch(error=>{
          console.log("error",error)
      })

      return;

  }
  res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
}


//   async zoomGetCode(req, res) {
//     if (req.query.code) {

//         // Step 3: 
//         // Request an access token using the auth code

//         let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;
//         let base64 = new Buffer(process.env.ZOOM_CLIENT_ID+':'+process.env.ZOOM_CLIENT_SECRET).toString('base64')

//         axios.post(url,{}, {
//             headers: {
//                 'Authorization': 'Basic '+base64

//             }
//         })
//         .then( (response) => {
//             // Parse response to JSON
//             // body = JSON.parse(body);
//             // console.log("response",response.data)
//             var body = response.data

//             // Logs your access and refresh tokens in the browser
//             console.log(`access_token: ${body.access_token}`);
//             console.log(`refresh_token: ${body.refresh_token}`);

//             if (body.access_token) {
              
//                 const config = {
                  
//                     headers: {'authorization': 'Bearer '+body.access_token}
//                 };

//                 // Step 4:
//                 // We can now use the access token to authenticate API calls

//                 // Send a request to get your user information using the /me context
//                 // The `/me` context restricts an API call to the user the token belongs to
//                 // This helps make calls to user-specific endpoints instead of storing the userID
//                 console.log("hello I am here")
              
//                 axios.get('https://api.zoom.us/v2/users/me',config).then(

//                 (response) => {
//                         const body = response.data
//                         // body = JSON.parse(body.data);
//                         // Display response in console
//                         console.log('API call ', body);
//                         // Display response in browser
//                         res.send({status: true});
                  
//                 }).catch((error)=>{
//                     console.log("error",error)
//                 })

//             } else {
//                 // Handle errors, something's gone wrong!
//             }

//         }).catch(error=>{
//             console.log("error",error)
//         })

//         return;

//     }
//     res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
//   }




export async function zoomGetRoomUrl(req: Request, res: Response) {


  console.log("api backend code",req.query.code)
  if (req.query.code) {

      // Step 3: 
      // Request an access token using the auth code

      let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;

      request.post(url, (error, response, body) => {

          // Parse response to JSON
          body = JSON.parse(body);

          // Logs your access and refresh tokens in the browser
          console.log(`access_token: ${body.access_token}`);
          console.log(`refresh_token: ${body.refresh_token}`);

          if (body.access_token) {

              // Step 4:
              // We can now use the access token to authenticate API calls

              // Send a request to get your user information using the /me context
              // The `/me` context restricts an API call to the user the token belongs to
              // This helps make calls to user-specific endpoints instead of storing the userID

              request.get('https://api.zoom.us/v2/users/meetings', (error, response, body) => {
                  if (error) {
                      console.log('API Response Error: ', error)
                  } else {
                      body = JSON.parse(body);
                      // Display response in console
                      console.log('API call ', body);
                      // Display response in browser
                      var JSONResponse = '<pre><code>' + JSON.stringify(body, null, 2) + '</code></pre>'
                      res.send(`
                          <style>
                              @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap');@import url('https://necolas.github.io/normalize.css/8.0.1/normalize.css');html {color: #232333;font-family: 'Open Sans', Helvetica, Arial, sans-serif;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}h2 {font-weight: 700;font-size: 24px;}h4 {font-weight: 600;font-size: 14px;}.container {margin: 24px auto;padding: 16px;max-width: 720px;}.info {display: flex;align-items: center;}.info>div>span, .info>div>p {font-weight: 400;font-size: 13px;color: #747487;line-height: 16px;}.info>div>span::before {content: "👋";}.info>div>h2 {padding: 8px 0 6px;margin: 0;}.info>div>p {padding: 0;margin: 0;}.info>img {background: #747487;height: 96px;width: 96px;border-radius: 31.68px;overflow: hidden;margin: 0 20px 0 0;}.response {margin: 32px 0;display: flex;flex-wrap: wrap;align-items: center;justify-content: space-between;}.response>a {text-decoration: none;color: #2D8CFF;font-size: 14px;}.response>pre {overflow-x: scroll;background: #f6f7f9;padding: 1.2em 1.4em;border-radius: 10.56px;width: 100%;box-sizing: border-box;}
                          </style>
                          <div class="container">
                              <div class="info">
                                  <img src="${body.pic_url}" alt="User photo" />
                                  <div>
                                      <span>Hello World!</span>
                                      <h2>${body.first_name} ${body.last_name}</h2>
                                      <p>${body.role_name}, ${body.company}</p>
                                  </div>
                              </div>
                              <div class="response">
                                  <h4>JSON Response:</h4>
                                  <a href="https://marketplace.zoom.us/docs/api-reference/zoom-api/users/user" target="_blank">
                                      API Reference
                                  </a>
                                  ${JSONResponse}
                              </div>
                          </div>
                      `);
                  }
              }).auth(null, null, true, body.access_token);

          } else {
              // Handle errors, something's gone wrong!
          }

      }).auth(process.env.ZOOM_CLIENT_ID, process.env.ZOOM_CLIENT_SECRET);

      return;

  }
  res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
}







export async function zoomCreateUser(req: Request, res: Response) {


  if (req.query.code) {

      // Step 3: 
      // Request an access token using the auth code

      let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;
      let base64 = new Buffer(process.env.ZOOM_CLIENT_ID+':'+process.env.ZOOM_CLIENT_SECRET).toString('base64')

      axios.post(url,{}, {
          headers: {
              'Authorization': 'Basic '+base64

          }
      })
      .then( (response) => {
          // Parse response to JSON
          // body = JSON.parse(body);
          // console.log("response",response.data)
          var body = response.data

          // Logs your access and refresh tokens in the browser
          console.log(`access_token: ${body.access_token}`);
          console.log(`refresh_token: ${body.refresh_token}`);

          if (body.access_token) {
              
              const config = {
                  
                  headers: {'authorization': 'Bearer '+body.access_token}
              };

              // Step 4:
              // We can now use the access token to authenticate API calls

              // Send a request to get your user information using the /me context
              // The `/me` context restricts an API call to the user the token belongs to
              // This helps make calls to user-specific endpoints instead of storing the userID
              console.log("hello I am here")
              
              axios.post('https://api.zoom.us/v2/users',{
                  "action": "create",
                  "user_info": {
                    "email": "aps19@gmails.com",
                    "type": 1,
                    "first_name": "Terry",
                    "last_name": "Jones"
                  }
              },config).then(
              async(response) => {
                      const body = response.data

                      console.log('API call ', body);
                      var result = await axios.get('https://api.zoom.us/v2/users',config).then(

                          (response) => {
                                  const body = response.data
          
                                  console.log('users list ', body);
                                  return body
                  
                              
                          }).catch((error)=>{
                              console.log("error",error)
                          })


                      console.log("result",result.users[0])

                      var req_body = {
                          "topic": "string",
                          "type": "integer",
                          "start_time": "string",
                          "duration": "integer",
                          "schedule_for": "string",
                          "timezone": "string",
                          "password": "string",
                          "agenda": "string",
                          "recurrence": {
                            "type": "integer",
                            "repeat_interval": "integer",
                            "weekly_days": "string",
                            "monthly_day": "integer",
                            "monthly_week": "integer",
                            "monthly_week_day": "integer",
                            "end_times": "integer",
                            "end_date_time": "string"
                          },
                          "settings": {
                            "host_video": "boolean",
                            "participant_video": "boolean",
                            "cn_meeting": "boolean",
                            "in_meeting": "boolean",
                            "join_before_host": "boolean",
                            "mute_upon_entry": "boolean",
                            "watermark": "boolean",
                            "use_pmi": "boolean",
                            "approval_type": "integer",
                            "registration_type": "integer",
                            "audio": "string",
                            "auto_recording": "string",
                            "enforce_login": "boolean",
                            "enforce_login_domains": "string",
                            "alternative_hosts": "string",
                            "global_dial_in_countries": [
                              "string"
                            ],
                            "registrants_email_notification": "boolean"
                          }
                        }
                      
                      
                      // axios.post('https://api.zoom.us/v2/users/'+result.users[0].id+'/meetings',{
                      axios.post('https://api.zoom.us/v2/users/me/meetings',{

                      },config).then(
                      
                      (response) => {
                              const body = response.data
                              // body = JSON.parse(body.data);
                              // Display response in console
                              console.log('new Room ', body);
                              // Display response in browser
                              res.send({status: true});
                          
                      }).catch((error)=>{
                          console.log("error",error)
                      })

      
                  
              }).catch((error)=>{
                  console.log("error",error)
              })




          } else {
              // Handle errors, something's gone wrong!
          }

      }).catch(error=>{
          console.log("error",error)
      })

      return;

  }
  res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
}


export async function zoomListUsers(req: Request, res: Response) {


  if (req.query.code) {

      // Step 3: 
      // Request an access token using the auth code

      let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;

  request.post(url,{},{
      auth:{
          client_id:process.env.ZOOM_CLIENT_ID,
          client_secret: process.env.ZOOM_CLIENT_SECRET
          }


          })
          .then((error, response, body) => {

          // Parse response to JSON
          body = JSON.parse(body);

          // Logs your access and refresh tokens in the browser
          console.log(`access_token: ${body.access_token}`);
          console.log(`refresh_token: ${body.refresh_token}`);

          if (body.access_token) {

              // Step 4:
              // We can now use the access token to authenticate API calls

              // Send a request to get your user information using the /me context
              // The `/me` context restricts an API call to the user the token belongs to
              // This helps make calls to user-specific endpoints instead of storing the userID

              axios.get('https://api.zoom.us/v2/users').then(

              (error, response, body) => {
                  if (error) {
                      console.log('API Response Error: ', error)
                  } else {
                      body = JSON.parse(body);
                      // Display response in console
                      console.log('API call ', body);
                      // Display response in browser
                      var JSONResponse = '<pre><code>' + JSON.stringify(body, null, 2) + '</code></pre>'
                      res.send(`
                          <style>
                              @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap');@import url('https://necolas.github.io/normalize.css/8.0.1/normalize.css');html {color: #232333;font-family: 'Open Sans', Helvetica, Arial, sans-serif;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;}h2 {font-weight: 700;font-size: 24px;}h4 {font-weight: 600;font-size: 14px;}.container {margin: 24px auto;padding: 16px;max-width: 720px;}.info {display: flex;align-items: center;}.info>div>span, .info>div>p {font-weight: 400;font-size: 13px;color: #747487;line-height: 16px;}.info>div>span::before {content: "👋";}.info>div>h2 {padding: 8px 0 6px;margin: 0;}.info>div>p {padding: 0;margin: 0;}.info>img {background: #747487;height: 96px;width: 96px;border-radius: 31.68px;overflow: hidden;margin: 0 20px 0 0;}.response {margin: 32px 0;display: flex;flex-wrap: wrap;align-items: center;justify-content: space-between;}.response>a {text-decoration: none;color: #2D8CFF;font-size: 14px;}.response>pre {overflow-x: scroll;background: #f6f7f9;padding: 1.2em 1.4em;border-radius: 10.56px;width: 100%;box-sizing: border-box;}
                          </style>
                          <div class="container">
                              <div class="info">
                                  <img src="${body.pic_url}" alt="User photo" />
                                  <div>
                                      <span>Hello World!</span>
                                      <h2>${body.first_name} ${body.last_name}</h2>
                                      <p>${body.role_name}, ${body.company}</p>
                                  </div>
                              </div>
                              <div class="response">
                                  <h4>JSON Response:</h4>
                                  <a href="https://marketplace.zoom.us/docs/api-reference/zoom-api/users/user" target="_blank">
                                      API Reference
                                  </a>
                                  ${JSONResponse}
                              </div>
                          </div>
                      `);
                  }
              }).auth(null, null, true, body.access_token);

          } else {
              // Handle errors, something's gone wrong!
          }

      })

      return;

  }
  res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
}





export async function zoomCreateUserSuccess(req: Request, res: Response) {


  if (req.query.code) {

      // Step 3: 
      // Request an access token using the auth code

      let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=' + process.env.redirectURL;
      let base64 = new Buffer(process.env.ZOOM_CLIENT_ID+':'+process.env.ZOOM_CLIENT_SECRET).toString('base64')

      axios.post(url,{}, {
          headers: {
              'Authorization': 'Basic '+base64

          }
      })
      .then( (response) => {
          // Parse response to JSON
          // body = JSON.parse(body);
          // console.log("response",response.data)
          var body = response.data

          // Logs your access and refresh tokens in the browser
          console.log(`access_token: ${body.access_token}`);
          console.log(`refresh_token: ${body.refresh_token}`);

          if (body.access_token) {
              
              const config = {
                  
                  headers: {'authorization': 'Bearer '+body.access_token}
              };

              // Step 4:
              // We can now use the access token to authenticate API calls

              // Send a request to get your user information using the /me context
              // The `/me` context restricts an API call to the user the token belongs to
              // This helps make calls to user-specific endpoints instead of storing the userID
              console.log("hello I am here")
              
              axios.post('https://api.zoom.us/v2/users',{
                  "action": "create",
                  "user_info": {
                    "email": "example@example.com",
                    "type": 1,
                    "first_name": "Terry",
                    "last_name": "Jones"
                  }
              },config).then(

              (response) => {
                      const body = response.data

                      console.log('API call ', body);

      
                  
              }).catch((error)=>{
                  console.log("error",error)
              })




          } else {
              // Handle errors, something's gone wrong!
          }

      }).catch(error=>{
          console.log("error",error)
      })

      return;

  }
  res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=' + process.env.ZOOM_CLIENT_ID + '&redirect_uri=' + process.env.redirectURL)
}



// export async function zoomCreateUserSuccess(req, res) {

//   var access_token = req.session.zoom_auth.access_token

//   const config = {
                  
//       headers: {'authorization': 'Bearer '+access_token}
//   };

  
//   // axios.post('https://api.zoom.us/v2/users/'+result.users[0].id+'/meetings',{
//   axios.post('https://api.zoom.us/v2/users/me/meetings',{

//   },config).then(
  
//       (response) => {
//               const body = response.data
//               // body = JSON.parse(body.data);
//               // Display response in console
//               console.log('new Room ', body);
//               // Display response in browser
//               res.send({status: true});
          
//       }).catch((error)=>{
//           console.log("error",error)
//       })

//   }



export async function zoomAddUser(req: Request, res: Response) {

      var access_token = req.session.zoom_auth.access_token
  
      const config = {
                      
          headers: {'authorization': 'Bearer '+access_token}
      };
  
      
      axios.post('https://api.zoom.us/v2/users',{
          "action": "create",
          "user_info": {
            "email": "msg2zinat@gmail.com",
            "type": 1,
            "first_name": "Sarosh",
            "last_name": "Jones"
          }
      },config).then(
      
          (response) => {
                  const body = response.data
                  // body = JSON.parse(body.data);
                  // Display response in console
                  console.log('new Room ', body);
                  // Display response in browser
                  res.send({status: true});
              
          }).catch((error)=>{
              console.log("error",error)
          })
  
      }




export async function zoomUserList(req: Request, res: Response) {

      var access_token = req.session.zoom_auth.access_token
  
      const config = {
                      
          headers: {'authorization': 'Bearer '+access_token}
      };
  
      
      axios.get('https://api.zoom.us/v2/users',config).then(
      
          (response) => {
                  const body = response.data
                  // body = JSON.parse(body.data);
                  // Display response in console
                  console.log('new Room ', body);
                  // Display response in browser
                  res.send({status: true});
              
          }).catch((error)=>{
              console.log("error",error)
          })
  
      }




export async function zoomMainUserCreateRoom(req: Request, res: Response) {

      var access_token = req.session.zoom_auth.access_token
  
      const config = {
                      
          headers: {'authorization': 'Bearer '+access_token}
      };
  
      
      axios.post('https://api.zoom.us/v2/users/me/meetings',{

      },config).then(
      
          (response) => {
                  const body = response.data
                  // body = JSON.parse(body.data);
                  // Display response in console
                  console.log('new Room ', body);
                  // Display response in browser
                  res.send({status: true});
              
          }).catch((error)=>{
              console.log("error",error)
          })
  
      }



export async function zoomDeleteUser(req: Request, res: Response) {

      var access_token = req.session.zoom_auth.access_token
  
      const config = {
                      
          headers: {'authorization': 'Bearer '+access_token}
      };
  
      
      axios.delete('https://api.zoom.us/v2/users/xfWof8wKRm-wrd70Yl1vaA',config).then(
      
          (response) => {
                  const body = response.data
                  // body = JSON.parse(body.data);
                  // Display response in console
                  console.log('new Room ', body);
                  // Display response in browser
                  res.send({status: true});
              
          }).catch((error)=>{
              console.log("error",error)
          })
  
      }

export async function zoomGetAccessToken(req: Request, res: Response) {

  const code = req.body.code

  var options = {
    method: 'POST',
    url: 'https://zoom.us/oauth/token',
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    data: {
      grant_type: 'authorization_code',
      client_id: process.env.ZOOM_CLIENT_ID,
      client_secret: process.env.ZOOM_CLIENT_SECRET,
      code: code,
      redirect_uri: 'https://localhost:3000'
    }
  };

  axios.request(options).then(function (response) {
    console.log(response.data);
  }).catch(function (error) {
    console.error(error);
  });



  // const result = axios.request({
  //   url: "/oauth/token?grant_type=authorization_code&code=${authorization_code}&redirect_uri=https://localhost:3000",
  //   method: "post",
  //   baseURL: "https://zoom.us/",
  //   auth: {
  //     username: process.env.ZOOM_CLIENT_ID, // This is the client_id
  //     password: process.env.ZOOM_CLIENT_SECRET // This is the client_secret
  //   },
  //   // data: {
  //   //   "grant_type": "authorization_code",
  //   //   "code":code,
  //   //   "redirect_uri":"https://localhost:3000"
 
  //   // },
  //   headers: { 
  //     "Content-Type": "application/x-www-form-urlencoded",
  //   }
  // }).then(respose => {
  //   console.log(respose);  
  // }).catch(result => {
  //   console.log("result",result)
  // })

  res.send("abcd")
}
