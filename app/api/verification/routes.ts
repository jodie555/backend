import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/useremailvalidation", Controller.userEmailValidation)
        router.post("/api/supplieremailvalidation", Controller.supplierEmailValidation)
}