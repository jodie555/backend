
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");

import { Request, Response } from 'express'





export async function userEmailValidation(req: Request, res: Response){
  const token = req.body.token
  const email = req.body.email


  Mongoose.set('useFindAndModify', false);
  let User = Mongoose.model("User");
  const result = await User.findOneAndUpdate({
      email: email,
      confirmationToken:token
    },

    {
      verifiedStatus: true
    }).exec()
  
    if (!result){
      throw new Exception(
        CONSTANT_ERROR.EMAIL_VALIDATION_ERROR.message,
        CONSTANT_ERROR.EMAIL_VALIDATION_ERROR.code
      )
    }

  res.send({
    message: "account confirmed"
  })
}


export async function supplierEmailValidation(req: Request, res: Response){
  const supplierToken = req.body.supplierToken
  const universityEmail = req.body.universityEmail


  // Mongoose.set('useFindAndModify', false);
  let User = Mongoose.model("User");
  const result = await User.findOneAndUpdate({
      universityEmail: universityEmail,
      confirmationSupplierToken:supplierToken
    },

    {
      verifiedSupplierStatus: true
    }).exec()

  if (!result){
    throw new Exception(
      CONSTANT_ERROR.EMAIL_VALIDATION_ERROR.message,
      CONSTANT_ERROR.EMAIL_VALIDATION_ERROR.code
    )
  }

  res.send({
    message: "account confirmed"
  })
}