import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
import {checkFacebookAccessToken} from "./functions"
import { Request, Response } from 'express'



export async function facebookLogin(req: Request, res: Response) {
  const token = await this.facebookOAuth2.getAccessTokenFromAuthorizationCodeFlow(req)

  const data = await checkFacebookAccessToken(token)
  console.log(data); // { id, email, first_name, last_name }
  
  if(data.email && data.id){
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: data.email });

    if (!user) {
      throw new Exception(
        CONSTANT_ERROR.UNAUTHENTICATED.message,
        CONSTANT_ERROR.UNAUTHENTICATED.code
      ).setDetail(__("invalid user"));
    } 
    req.session.auth = user;
    return res.send({
      status: true,
      _id:user._id,
      email: user.email,
      name: user.name,
      picture: user.picture,
      isSupplier: user.isSupplier,
      verifiedSupplierStatus: user.verifiedSupplierStatus,
    })
    
  }
  // if later you need to refresh the token you can use
  // const newToken = await this.getNewAccessTokenUsingRefreshToken(token.refresh_token)
  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid login"));
}



export async function facebookRegisterUser(req: Request, res: Response) {
  const { token } = req.body;
  const data = await checkFacebookAccessToken(token)

  // check whether the email is verified,
  if (data.email) {
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: data.email });
    var randomstring = Math.random().toString(36).slice(-8);

    if (!user) {
      let newuser = new User();
      newuser.email = data.email;
      newuser.username = data.name;
      newuser.password = randomstring;
      newuser.verifiedStatus = true;
      newuser.createAt = new Date();
      newuser = await newuser.save();

      user = newuser;
      req.session.auth = user;
      return res.send({
        status: true,
        email: user.email,
        name: user.name,
        picture: user.picture,
      });
    } else {
      req.session.auth = user;
      return res.send({
        status: true,
        _id:user._id,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });
    }
  }

  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid login"));
}


export async function facebookRegisterSupplier(req: Request, res: Response) {
  const { token } = req.body;
  const data = await checkFacebookAccessToken(token)

  // check whether the email is verified,
  if (data.email) {
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: data.email });
    var randomstring = Math.random().toString(36).slice(-8);

    if (!user) {
      let newuser = new User();
      newuser.email = data.email;
      newuser.username = name;
      newuser.password = randomstring;
      newuser.createAt = new Date();
      newuser.verifiedStatus = true;
      newuser.isSupplier = true
      newuser = await newuser.save();

      user = newuser;
      return res.send({
        email: user.email,
        name: user.name,
        picture: user.picture,
      });
    } else {
      
      // if the user is not yet a supplier,
      // we will update his information
      user.isSupplier = true
      user = await user.save()


      req.session.auth = user;
      return res.send({
        status: true,
        _id:user._id,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });

    }
  }

  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid login"));
}

//   async facebookRegisterSupplierAddInfo(req, res) {
//     const { token, university, universityMajor, universityEmail } = req.body;
//     if (
//       !req.body ||
//       !token ||
//       !university ||
//       !universityMajor ||
//       !universityEmail
//     ) {
//       const error = new Error("Invalid body");
//       error.status = 422;
//       error.type = "InvalidInput";
//       throw error;
//     }

//     const ticket = await client.verifyIdToken({
//       idToken: token,
//       audience: process.env.CLIENT_ID,
//     });
//     const { name, email, picture, email_verified } = ticket.getPayload();

//     // check whether the email is verified,
//     if (email_verified === true) {
//       const avatar = req.file;
//       console.log("email", email);

//       let avatarUrl = null;
//       if (avatar) {
//         avatarUrl = avatar.path.replace(/\\/g, "/");
//       }

//       let User = Mongoose.model("User");
//       let user = await User.findOne({ email: email, registerFinished: false });

//       console.log("user", user);

//       if (user) {
//         user.universityEmail = universityEmail;
//         user.universityMajor = universityMajor;
//         user.university = university;
//         user.avatarUrl = avatarUrl;
//         user.registerFinished = true;
//         user.isSupplier = true;
//         user = await user.save();

//         req.session.auth = user;
//         return res.send({ email: user.email, name: user.name });
//       } else {
//         const error = new Error("Not Valid");
//         error.status = 401;
//         error.type = "NotValid";
//         throw error;
//       }
//     }

//     const error = new Error("Unauthorized");
//     error.status = 401;
//     error.type = "Unauthorized";
//     throw error;
//   }