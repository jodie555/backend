import * as Controller  from "./controllers"

module.exports = router => {
        router.get("/api/facebook/login", Controller.facebookLogin)
        router.post("/api/facebook/register/user", Controller.facebookRegisterUser)
        router.post("/api/facebook/register/supplier", Controller.facebookRegisterSupplier)
        // fastify.post("/api/google/register/supplier/add-additional-info",{ preHandler: upload.single('avatar') }, Controller.googleRegisterSupplierAddInfo)

}