const axios = require("axios");


export async function checkFacebookAccessToken(token){


    const { data } = await axios({
        url: 'https://graph.facebook.com/me',
        method: 'get',
        params: {
          fields: ['id', 'email', 'name'].join(','),
          access_token: token.access_token,
        },
      });

    return data
}