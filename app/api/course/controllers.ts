const Mongoose = require("mongoose");
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";
import { Request, Response } from 'express'

export async function getCourses(req: Request, res: Response) {
    let Course = Mongoose.model("Course");
    let coursesResult = await Course.find({
      university: req.query.university,
      subject: req.query.subject,
    }).select("name");
    res.send(coursesResult);
  }

export async function createNewCourse(req: Request, res: Response) {
    // if (!req.body || !req.body.name) {

    // }
    console.log(req.body.name)
    const subjectName = req.body.name;
    var subjectImageURL = "";
    if (req.body.imageURL) {
      subjectImageURL = req.body.imageURL;
    }
    var subjectDescription = "";
    if (req.body.description) {
      subjectDescription = req.body.description;
    }

    let Subject = Mongoose.model("Subject");

    //   check whether the input body is valid
    const doesSubjectExist = await Subject.exists({ name: subjectName });
    console.log(doesSubjectExist);

    if (doesSubjectExist) {
      throw new Exception(
        CONSTANT_ERROR.DB_READ_ERROR.message,
        CONSTANT_ERROR.DB_READ_ERROR.code
      ).setDetail(__("subject does not exist"));
    }

    let newSubject = new Subject();
    newSubject.name = subjectName;
    newSubject.imageURL = subjectImageURL;
    newSubject.description = subjectDescription;

    newSubject = await newSubject.save().catch((err) => {
      throw new Exception(
        CONSTANT_ERROR.DB_SAVE_ERROR.message,
        CONSTANT_ERROR.DB_SAVE_ERROR.code
      ).setDetail(__(err.message));
    });

    res.send({
      status: true,
      subjectId: newSubject._id,
    });
  }

