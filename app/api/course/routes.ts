import * as Controller  from "./controllers"
import * as Validator from "./validator"

module.exports = router => {
        router.get("/api/courses", Controller.getCourses);

        // need to add image uploading feature to this controller
        //this is for creating new subject and not course
        router.post("/api/courses",[...Validator.courseBody], Controller.createNewCourse); 
}