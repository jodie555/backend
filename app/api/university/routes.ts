import * as Controller  from "./controllers"

module.exports = router => {
        router.get("/api/universities", Controller.getUniversities);
        router.post("/api/universities", Controller.createNewUniversity);

}