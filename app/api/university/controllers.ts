
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");

import { Request, Response } from 'express'


export async function getUniversities(req: Request, res: Response){
  let University = Mongoose.model("University")
  let universitiesResult = await University.find({}).select('name')
  res.send(universitiesResult)
}

export async function createNewUniversity(req, res){
  
  // if( !req.body
  //     ||!req.body.name){

  //         ErrorMessages.invalidBody()
      
  //   }

  const universityName = req.body.name
  const universityImageURL = req.body.imageURL
  const universityDescription = req.body.description
  const universityURL = req.body.url

  // const universityImageURL = ""
  // if (req.body.imageURL){
  //     universityImageURL = req.body.imageURL
  // }
  // const universityDescription = ""
  // if (req.body.description){
  //     universityDescription = req.body.description
  // }
  // const universityURL = ""
  // if (req.body.url){
  //     universityURL = req.body.url
  // }


  let University = Mongoose.model("University");
  
  //   check whether the input body is valid
  const doesUniversityExist = await University.exists({name:universityName})

  if (doesUniversityExist){
    throw new Exception(
      CONSTANT_ERROR.DB_READ_ERROR.message,
      CONSTANT_ERROR.DB_READ_ERROR.code
    ).setDetail(__("cannot find university"));
  }

  let newUniversity = new University();
  newUniversity.name = universityName
  newUniversity.imageURL = universityImageURL
  newUniversity.description = universityDescription
  newUniversity.url = universityURL
  
  newUniversity = await newUniversity
      .save()
      .catch(err => {
        throw new Exception(
          CONSTANT_ERROR.DB_SAVE_ERROR.message,
          CONSTANT_ERROR.DB_SAVE_ERROR.code
        ).setDetail(__(err.message));
          
      })

  res.send({
      status: true,
      universityId:newUniversity._id,
  })
}