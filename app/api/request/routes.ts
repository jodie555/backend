import Auth from "../../middlewares/auth";
import * as Controller  from "./controllers"

module.exports = router => {
        
        router.get("/api/requests/all", Controller.getAllRequests); // for test only
        router.get("/api/requests",[Auth.api], Controller.getRequests);//no use

        // fastify.get("/api/requests/:supplierId", Controller.getAllRequestsBySupplierId);
        // fastify.post("/api/requests", Controller.createNewRequest);
        router.patch("/api/requests/:_id/add-chatroom", Controller.updateChatRoomIdAsUser);//no use

        // to do
        router.patch("/api/requests/:_id/add-transaction", Controller.updateChatRoomIdAsUser);//no use

        router.get("/api/requests/supplier/status",[Auth.api], Controller.getRequestsByStatusNumAsSupplier);
        router.get("/api/requests/supplier/possible-requests", Controller.getAllRequestsAsPossibleSupplier);
        
        router.get("/api/requests/user", Controller.getAllRequestsAsUser);//no use
        
        // supplier can use this api to get all the requests that he is involved
        router.get("/api/requests/supplier", Controller.getAllRequestsAsSupplier);//no use


        router.get("/api/requests/user/no-supplier", Controller.getAllRequestsNoSupplierAsUser);// no use

        router.get("/api/requests/user/status", Controller.getRequestsByStatusNum);

        //get accepted supplier by requestID
        //the target request should be owned by the user (the controller will also check the session._id)
        router.get("/api/requests/:_id/accepted-suppliers", Controller.getAcceptedSuppliers);

        router.get("/api/requests/:_id", Controller.getRequestById);

        router.get("/api/requests/:conversationSID/chatroom-info", Controller.getRequestByConversationSID);

        router.post("/api/requests/user",[Auth.api], Controller.createNewRequestByUser); 
//         fastify.patch("/api/requests/:_id/user/add-supplier", Controller.addSupplierIdAsUser);
//         fastify.patch("/api/requests/:_id/supplier/add-to-accepted-list", Controller.addSupplierIdToAcceptedList);

//         fastify.patch("/api/requests/:_id/end-request", Controller.endRequest);


//         // fastify.delete("/api/auth/request/delete_request", Controller.deleteRequest);
//         // fastify.update("/api/auth/request/update_request", Controller.updateRequest);

//         // API for testing
//         fastify.patch("/api/requests/:_id/testing", Controller.changeChatroomSupplier);

        router.get("/api/requests/supplier/matchedUsers",[Auth.api], Controller.getMatchedUsers);
}