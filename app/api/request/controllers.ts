const Mongoose = require("mongoose");
const ObjectId = Mongoose.Types.ObjectId;
// const Validation = require("../../functions/validations")
// const ErrorMessages = require("../../functions/errorMessages")
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { Request, Response } from 'express'
import { __ } from 'i18n'




// for test only 
export async function getAllRequests(req: Request, res: Response){
    let Request = Mongoose.model("Request")
    let requestsResult = await Request.find()
    //let requestsResult = await Request.find({userId: "6069a88b6faa36542841c58e"})
    res.send(requestsResult)

}


export async function getRequests(req: Request, res: Response){
    let Request = Mongoose.model("Request")
    if (req.session.auth !== undefined){
        let requestsResult = await Request.find({userId:req.session.auth._id})
        //let requestsResult = await Request.find({userId: "6069a88b6faa36542841c58e"})
        res.send(requestsResult)
    }else{
        throw new Exception(
            CONSTANT_ERROR.UNAUTHENTICATED.message,
            CONSTANT_ERROR.UNAUTHENTICATED.code
            ).setDetail("no session auth");
    }


}

export async function getAllRequestsBySupplierId(req: Request, res: Response){
        
        // Validation.sessionAuth(req)


        // if(!req.params.supplierId){

        //     ErrorMessages.invalidParams()
        // }

        const supplierId = req.params.supplierId
        
        let User = Mongoose.model("User");
        const doesSupplierExist = User.exists({_id:supplierId,isSupplier: true})


        // if (!doesSupplierExist){
        //     ErrorMessages.documentNotFound()
        // }
        
        let Request = Mongoose.model("Request");
        const result = await Request.find({supplierId: supplierId});
        res.send(result);
    }


export async function createNewRequest(req: Request, res: Response){

        // if(req.session.auth == undefined ){

        //     const error = new Error('Unauthorized')
        //     error.status = 401;
        //     error.type= "Unauthorized"
        //     throw error;
        //   } 

        // if( !req.body
        //     ||!req.body.userId
        //     ||!req.body.supplierId
        //     ||!req.body.status
        //     ||!req.body.description){

        //         ErrorMessages.invalidBody()
            
        //   }

        const status = req.body.status
        const userId = req.body.userId
        const supplierId = req.body.supplierId
        const description = req.body.description


        let User = Mongoose.model("User");

        const doesSupplierExist = await User.exists({_id:supplierId,isSupplier: true})

        if (!doesSupplierExist ){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              )
        }

        let Request = Mongoose.model("Request");

        let newRequest = new Request()
        newRequest.status = status
        newRequest.userId = userId
        newRequest.supplierId = supplierId
        newRequest.description =  description
        newRequest.requestCreatedTime =  new Date()
        newRequest = await newRequest
            .save()
            .catch(err => {
                throw new Exception(
                    CONSTANT_ERROR.DB_SAVE_ERROR.message,
                    CONSTANT_ERROR.DB_SAVE_ERROR.code
                  ).setDetail(err.message);
                
            })

        res.send({
            status: true,
            data: {},
        })
        // res.send(newRequest)
    }


export async function createNewRequestByUser(req: Request, res: Response){


        // Validation.sessionAuth(req)
        
        // if( !req.body
        //     ||!req.body.universityId
        //     ||!req.body.subjectId
        //     ||!req.body.description){

        //         ErrorMessages.invalidBody()

            
        //   }

        const userId = req.session.auth._id

        let User = Mongoose.model("User");
        

        // find possible supplier and add them to the list in the request
        /**
         * this need to be editted further to sort out certain supplier
         */
         const doesUserExist = User.exists({_id:userId})
        

        if (!doesUserExist){
            throw new Exception(
                CONSTANT_ERROR.DB_SAVE_ERROR.message,
                CONSTANT_ERROR.DB_SAVE_ERROR.code
              )
        }


        let Request = Mongoose.model("Request");

        let RequestExist = await Request.findOne({userId:userId, supplierId:req.body.supplierId}).exec()
        if (RequestExist){
            return res.send({
                status: true,
                request:RequestExist,
            })
        }
       
        let newRequest = new Request()
        newRequest.userId = userId
        newRequest.supplierId = req.body.supplierId
        newRequest.requestCreatedTime =  new Date()
        newRequest = await newRequest
            .save()
            .catch(err => {
                throw new Exception(
                    CONSTANT_ERROR.DB_SAVE_ERROR.message,
                    CONSTANT_ERROR.DB_SAVE_ERROR.code
                  ).setDetail(err.message);
                
            })

        res.send({
            status: true,
            requestId:newRequest,
        })

    }




    
export async function getRequestsByStatusNumAsSupplier(req: Request, res: Response){
        // Validation.sessionAuth(req)

        const supplierId = req.session.auth._id
        const status = req.query.statusNum

    
        let User = Mongoose.model("User");
        const doesSupplierExist = User.exists({_id:supplierId,isSupplier: true})


        if (!doesSupplierExist){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("supplier_does_not_exist")

        }
        
        let Request = Mongoose.model("Request");
        if (status){
            const result = await Request.find({supplierId: supplierId,status:status}).
            populate('university', 'name').
            populate('subject', 'name').
            populate('userId', 'firstName').
            populate('chatroomId', 'conversationSID createAt')
            res.send(result);
        }else{
            // const result = await Request.find({userId: userId}).
            // populate('university', 'name').
            // populate('subject', 'name').
            // populate('chatroomId', 'conversationSID')
            // res.send(result);
        }
    }



    
export async function getAllRequestsAsPossibleSupplier(req: Request, res: Response){
        // Validation.sessionAuth(req)

        const supplierId = req.session.auth._id

    
        let User = Mongoose.model("User");
        const doesSupplierExist = await User.exists({_id:supplierId,isSupplier: true})


        if (!doesSupplierExist){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              )

        }
        
        let Request = Mongoose.model("Request");
        // const result = await Request.find({possibleSuppliers: supplierId, supplierId:{$exists: false}}).where('acceptedSuppliers').ne("6069f21e8b27eeb146fe0765");
        // const result = await Request.find({possibleSuppliers:{$in:[supplierId]},supplierId:{$exists: false},acceptedSuppliers:{$nin:[supplierId]}})
        const result = await Request.find({$and:[
            {possibleSuppliers:{$in:[supplierId]}},
            {supplierId:{$exists: false}}
            // {acceptedSuppliers:{$nin:[supplierId]}}
        ]}).populate('university', 'name')
        .populate('subject', 'name')

        res.send(result);
    }


export async function getAllRequestsAsUser(req: Request, res: Response){
        // Validation.sessionAuth(req)

        const userId = req.session.auth._id

        let Request = Mongoose.model("Request");
        const result = await Request.find({userId: userId});
        res.send(result);
    }


export async function getAllRequestsAsSupplier(req: Request, res: Response){
    // Validation.sessionAuth(req)
    const supplierId = req.session.auth._id

    let Request = Mongoose.model("Request");
    const result = await Request.find({supplierId: supplierId});
    res.send(result);
}





export async function getAcceptedSuppliers(req: Request, res: Response){
        // Validation.sessionAuth(req)

        const userId = req.session.auth._id

        
        let Request = Mongoose.model("Request");
        const RequestResult = await Request.findOne({_id:req.params._id,userId: userId})
                                    .select('acceptedSuppliers')
        console.log(userId)
        const acceptedSuppliers = RequestResult.acceptedSuppliers

        let User = Mongoose.model("User");  
        const result = await User.find({_id:acceptedSuppliers})
                                    .populate({
                                        path: 'universityIdentities',
                                        select: 'university course',
                                        populate: {
                                            path: 'university course',
                                            select: 'name name'
                                        }
                                    });

        res.send(result);
    }




export async function getAllRequestsNoSupplierAsUser(req: Request, res: Response){
        // Validation.sessionAuth(req)


        const userId = req.session.auth._id

        
        let Request = Mongoose.model("Request");
        const result = await Request.find({userId: userId,supplierId:null});
        res.send(result);
    }



export async function getRequestsByStatusNum(req: Request, res: Response){

        // Validation.sessionAuth(req)
    

        const userId = req.session.auth._id
        const status = req.query.statusNum

        if(status){
            let Request = Mongoose.model("Request");
            let University = Mongoose.model("University");
            var result = await Request.find({userId: userId,status:status}).
                                populate('university', 'name').
                                populate('subject', 'name').
                                populate('chatroomId', 'conversationSID createAt endTime');
            res.send(result);
        }else{
            let Request = Mongoose.model("Request");
            const result = await Request.find({userId: userId});
            res.send(result);
        }

        

    }

export async function getRequestById(req: Request, res: Response){
        // Validation.sessionAuth(req)

        let Request = Mongoose.model("Request");
        const result = await Request.findOne({_id: req.params._id});
        res.send(result);
    }

export async function getRequestByConversationSID(req: Request, res: Response){
        // if(req.session.auth == undefined ){
        //     const error = new Error('Unauthorized')
        //     error.status = 401;
        //     error.type= "Unauthorized"
        //     throw error;
        // }
        let Chatroom = Mongoose.model("Chatroom");
        const chatroom = await Chatroom.findOne({conversationSID: req.params.conversationSID});
        let Request = Mongoose.model("Request");
        const request = await Request.findOne({chatroomId: chatroom._id})
                                        .populate({path:'supplierId', select:'firstName universityIdentities',
                                            populate: {
                                                path: 'universityIdentities',
                                                select: 'university course',
                                                populate: {
                                                    path: 'university course',
                                                    select: 'name name'
                                                }
                                            }}).exec();
        // const request = await Request.findOne({chatroomId: chatroom._id})
        //                                 .populate({path:'supplierId', select:'firstName university universityMajor',
        //                                     populate: {
        //                                         path: 'university universityMajor',
        //                                         select: 'name name'
        //                                     }});
        
        res.send(request);
    }


export async function updateChatRoomIdAsUser(req: Request, res: Response){


        if(!req.params._id){

            throw new Exception(
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
              ).setDetail(__('empty_userId'))
        }


        // if( !req.body
        //     ||!req.body.chatroomId){

        //     ErrorMessages.invalidBody()

            
        //   }

        const requestId = req.params._id
        
        
        let Request = Mongoose.model("Request");
        const request = await Request.findOne({_id: requestId});
        request.chatroomId = req.body.chatroomId
        const updatedChatroom = await request.save()
        res.send(updatedChatroom);
    }



export async function addSupplierIdAsUser(req: Request, res: Response){
        // Validation.sessionAuth(req)

        if(!req.params._id){
            throw new Exception(
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
              ).setDetail(__('empty_userId'))

        }


        // if( !req.body
        //     ||!req.body.supplierId){
        //     ErrorMessages.invalidBody()

        //   }
        
        const requestId = req.params._id
        const userId = req.session.auth._id
        const supplierId = req.body.supplierId



        // check whether the input supplier exist 
        let User = Mongoose.model("User");
        const doesSupplierExist = await User.exists({_id:supplierId,isSupplier: true})
        
        if (!doesSupplierExist){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("cannot find supplier")
        }

        // check whether the user owns the request   
        let Request = Mongoose.model("Request");
        const request = await Request.findOne({_id: requestId,userId:userId});

        if (!request){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("cannot find request")
        }

        if(request.supplierId){
            res.send({
                status: true,
                data:{},
                message:"supplier was already added"
            })
        }


        request.supplierId = supplierId
        request.status = 1
        await request.save()
        res.send({
            status: true,
            data:{},
        })


    }



export async function addSupplierIdToAcceptedList(req: Request, res: Response){
        // Validation.sessionAuth(req)


        if(!req.params._id){

            throw new Exception(
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
                CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
              ).setDetail(__('empty_userId'))

        }


        const requestId = req.params._id
        const supplierId = req.session.auth._id




        // check whether the supplier exist 
        let User = Mongoose.model("User");
        const doesSupplierExist = await User.exists({_id:supplierId,isSupplier: true})
        
        if (!doesSupplierExist){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("cannot find supplier")
        }

        // check whether the request exist  
        let Request = Mongoose.model("Request");
        const request = await Request.findOne({_id: requestId});

        if (!request){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("cannot find supplier")
        }

        // prevent duplicate supplier in the accepted supplier list

        if(request.acceptedSuppliers.includes(supplierId)){
            return res.send({
                status: true,
                message:"already exist",
            })
        }



        request.acceptedSuppliers.push(supplierId)
        await request.save()
        res.send({
            status: true,
            data:{},
        })


    }

/**
 * this is for test only
 * @param req 
 * @param res 
 */

export async function changeChatroomSupplier(req: Request, res: Response){
        let Request = Mongoose.model("Request");
        const request = await Request.findOne({_id: req.params._id});
        request.supplierId = "608439ca54937d4ce4058a1b";
        const updatedRequest = await request.save();
        res.send(updatedRequest);
    }

export async function endRequest(req: Request, res: Response){
        const requestId = req.params._id
        // Change request status to 2
        let Request = Mongoose.model("Request");
        let request = await Request.findOne({_id: requestId});
        request.status = 2
        await request.save()

        // Add chatroom endtime
        let Chatroom = Mongoose.model("Chatroom");
        console.log(request.chatroomId)
        let chatroom = await Chatroom.findOne({_id: request.chatroomId});
        if(!chatroom){
            throw new Exception(
                CONSTANT_ERROR.DB_READ_ERROR.message,
                CONSTANT_ERROR.DB_READ_ERROR.code
              ).setDetail("cannot find supplier")
        }


        chatroom.endTime = new Date()

        await chatroom.save()
            .catch(err => {
                throw new Exception(
                    CONSTANT_ERROR.DB_SAVE_ERROR.message,
                    CONSTANT_ERROR.DB_SAVE_ERROR.code
                  ).setDetail(err.message);
        })
    
        res.send({
            status: true,
            data:{},
        })
    }

export async function getMatchedUsers(req: Request, res: Response){
        const supplierId = req.session.auth._id
        console.log("dfsdf",supplierId)
        let Request = Mongoose.model("Request");
        const results = await Request.find({supplierId:supplierId})
        console.log("results",results)
        res.send(results);
    }




