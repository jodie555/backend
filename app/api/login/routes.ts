import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/login/user",  Controller.userLogin)
        router.get("/api/login/supplier", Controller.userLogin)
        router.get("/api/login/supplier/test", Controller.userLogin)

}