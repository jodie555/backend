
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
// const ErrorMessages = require("../../functions/errorMessages")
import { Request, Response } from 'express'



export async function userLogin(req: Request, res: Response) {
  console.log(req.session.auth)
  console.log(req.body)
  let User = Mongoose.model("User");
  let user = await User.findOne({ email: req.body.email }).select(
    "password _id isSupplier"
  );
  console.log(user)

  if (!user || !bcrypt.compareSync(req.body.password, user.password)) {
    // ErrorMessages.documentNotFound()
    throw new Exception(
      CONSTANT_ERROR.UNAUTHENTICATED.message,
      CONSTANT_ERROR.UNAUTHENTICATED.code
    ).setDetail(__("password not match"));

  } else {
    req.session.auth = user

    res.send({
      status: true,
      _id: user._id,
      isSupplier: user.isSupplier,
    });
  }

}