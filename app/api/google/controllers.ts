const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);// be ware ,process.env.CLIENT_ID is empty
import { CONSTANT_ERROR, Exception } from "../../exception/Exception";
import { __ } from "i18n";

const Mongoose = require("mongoose");
const bcrypt = require("bcrypt");
// const ErrorMessages = require("../../functions/errorMessages")
import { Request, Response } from 'express'



export async function googleLogin(req: Request, res: Response) {
  const { token } = req.body;
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const { name, email, picture, email_verified } = ticket.getPayload();
  console.log(email)
  // check whether the email is verified,
  if (email_verified === true) {
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: email });

    if (!user) {
      throw new Exception(
        CONSTANT_ERROR.UNAUTHENTICATED.message,
        CONSTANT_ERROR.UNAUTHENTICATED.code
      ).setDetail(__("invalid user"));

    } 
    req.session.auth = user;
    console.log(req.session.auth)
    return res.send({
      status: true,
      _id:user._id,
      email: user.email,
      name: user.name,
      picture: user.picture,
      isSupplier: user.isSupplier,
      verifiedSupplierStatus: user.verifiedSupplierStatus,
    });
    
  }

  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid email"));

}



export async function googleRegister(req: Request, res: Response) {
  const { token } = req.body;
  const registerAsSupplier = req.query.registerAsSupplier
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const { name, email, picture, email_verified } = ticket.getPayload();

  // check whether the email is verified,
  if (email_verified === true) {
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: email });
    var randomstring = Math.random().toString(36).slice(-8);

    if (!user) {
      let newuser = new User();
      newuser.email = email;
      newuser.username = name;
      newuser.password = randomstring;
      newuser.verifiedStatus = true;
      newuser.createAt = new Date();
      registerAsSupplier? newuser.isSupplier = true : null
      newuser = await newuser.save();

      user = newuser;
      req.session.auth = user;
      return res.send({
        status: true,
        _id:user._id,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });
    } else {

      if(registerAsSupplier && user.isSupplier == false){
        console.log("updated",registerAsSupplier)
        user.isSupplier = true
        user = await user.save();
      }


      req.session.auth = user;
      return res.send({
        status: true,
        _id:user._id,
        registerAsSupplier: true,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });
    }
  }

  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid email"));

}



export async function googleRegisterSupplier(req: Request, res: Response) {
  const { token } = req.body;
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const { name, email, picture, email_verified } = ticket.getPayload();

  // check whether the email is verified,
  if (email_verified === true) {
    let User = Mongoose.model("User");
    let user = await User.findOne({ email: email });
    var randomstring = Math.random().toString(36).slice(-8);

    if (!user) {
      let newuser = new User();
      newuser.email = email;
      newuser.username = name;
      newuser.password = randomstring;
      newuser.createAt = new Date();
      newuser.verifiedStatus = true;
      newuser.isSupplier = true
      newuser = await newuser.save();

      user = newuser;
      return res.send({
        status:true,
        _id:user._id,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });
    } else {

      // if the user is not yet a supplier,
      // we will update his information
      user.isSupplier = true
      user = await user.save()

      req.session.auth = user;
      return res.send({
        status: true,
        _id:user._id,
        email: user.email,
        name: user.name,
        picture: user.picture,
        isSupplier: user.isSupplier,
        verifiedSupplierStatus: user.verifiedSupplierStatus,
      });
    }
  }

  throw new Exception(
    CONSTANT_ERROR.UNAUTHENTICATED.message,
    CONSTANT_ERROR.UNAUTHENTICATED.code
  ).setDetail(__("invalid email"));

}