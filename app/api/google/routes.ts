import * as Controller  from "./controllers"

module.exports = router => {
        router.post("/api/google/login", Controller.googleLogin)
        router.post("/api/google/register", Controller.googleRegister)
        router.post("/api/google/register/supplier", Controller.googleRegisterSupplier)
        // fastify.post("/api/google/register/supplier/add-additional-info",{ preHandler: upload.single('avatar') }, Controller.googleRegisterSupplierAddInfo)

}