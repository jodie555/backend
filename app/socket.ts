import { CONSTANT_ERROR, Exception } from "./exception/Exception";
let io

module.exports = {
  init: httpServer => {
    io = require('socket.io')(httpServer,{
      cors: {
        origin: '*',
      }
    })
    return io
  },
  getIO: () => {
    if(!io){
      throw new Exception(
        CONSTANT_ERROR.SOCKET_ERROR.message,
        CONSTANT_ERROR.SOCKET_ERROR.code
      ).setDetail('Socket.io not initialized!');

    }
    return io;
  }
}