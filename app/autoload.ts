'use strict'

import fs from 'fs'
import path from 'path'

function dir_require(dir, ignores = []) {
  for (let target of fs.readdirSync(dir)) {
    if (ignores.includes(target)) {
      continue
    }
    require(path.join('', dir, target))
    //require(path.join(dir, target))
  }
}

//  Util
// dir_require('./util')
dir_require(__dirname + '/util')

// Custom Global Module include
// require('./exception/Exception')
import './exception/Exception'
