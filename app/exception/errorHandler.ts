'use strict'

// var logger = require('../logger')
import logger from '../logger'
import { CONSTANT_ERROR } from './Exception'
function response(err, req, res, next) {
  console.log("err",err)
  // console.log("req",req)
  // console.log("res",res)
  let response = {
    result: 'fail',
    error: {
      code: err.code == null ? '99999' : err.code,
      message: err.message,
      detail: err.detail,
    },
    stack: process.env.APP_DEBUG
      ? (function (errStack) {
          return errStack.toString().split('\n')
        })(err.stack)
      : {},
  }
  logger.error(
    'Request::[' +
      req.ip +
      ']-[' +
      req.method +
      ']-[' +
      req.originalUrl +
      ']-[' +
      req.hostname +
      ']-[' +
      response.error.code +
      ']-[' +
      response.error.message +
      ']-[' +
      response.error.detail +
      ']'
  )
  switch (err.code) {
    case CONSTANT_ERROR.NOT_FOUND.code:
      res.status(404)
      break
    case CONSTANT_ERROR.UNAUTHENTICATED.code:
      res.status(401)
      break
    default:
      res.status(400)
      break
  }
  res.json(response)
}

export default {
  response: response,
}
