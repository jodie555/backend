'use strict'

import { __, getLocale } from 'i18n'
export class Exception extends Error {
  code: Number
  detail: String
  constructor(message, code) {
    super(__(message))
    this.code = code
  }

  /**
   * @param message : string
   * @returns {Exception}
   */
  setMessage(message) {
    this.message = __(message)
    return this
  }

  /**
   * @param code : string|number
   * @returns {Exception}
   */
  setCode(code) {
    this.code = code
    return this
  }

  setDetail(detail) {
    this.detail = detail
    return this
  }
}

export const CONSTANT_ERROR = {
  UNAUTHENTICATED: { message: 'unauthenticated', code: '10020' },
  USER_EXIST: { message: 'user_is_registered', code: '10030' },

  REQUEST_WITHOUT_NONCE: {
    message: 'request_without_nonce',
    code: '10040',
  },
  REQUEST_NONCE_INVALID: {
    message: 'request_nonce_invalid',
    code: '10050',
  },
  REQUEST_UNSIGNED: { message: 'request_unsigned', code: '10060' },
  APPLICATION_NOT_FOUND: {
    message: 'application_not_found',
    code: '10070',
  },
  EMAIL_VALIDATION_ERROR: {message: 'user_is_registered', code: '10080' },

  REQUEST_EXPIRED: { message: 'request_expired', code: '10090' },
  NOT_LOG_IN:{message:'not_logged_in',code:'10100'},
  SYSTEM_TEST: { message: 'system_test', code: '11000' },
  DB_SAVE_ERROR: { message: 'database_save_error', code: '12000' },
  DB_READ_ERROR: { message: 'database_read_error', code: '12100' },
  DB_DELETE_ERROR: { message: 'database_delete_error', code: '12100' },

  NOT_FOUND: { message: 'not_found', code: '0' },
  PARAM_VALIDATION_ERROR: {
    message: 'param_validation_Error',
    code: '13000',
  },
  PATH_IS_ERROR: { message: 'path_is_error', code: '14000' },
  PARKING_ERROR: { message: 'parking_error', code: '16000' },
  PRICE_SETTING_ERROR: { message: 'price_setting_error', code: '17000' },
  COMMON_VEHICLE_ERROR: { message: 'common_vehicle_error', code: '18000' },
  USERNAME_UNIQUE: { message: 'username_unique', code: '10080' },
  DO_NOT_HAVE_THE_AUTHORITY: {
    message: 'do_not_have_the_authority',
    code: '10090',
  },
  MONTHLY_RECORD_CREATE_ERROR: {
    message: 'monthly_record_create_error',
    code: '19000',
  },
  PRINT_ERROR: {
    message: 'print_error',
    code: '20000',
  },
  CALL_CLOUD_API_ERROR: {
    message: 'call_cloud_api_error',
    code: '21000',
  },
  CANT_EDIT_FOR_VEHICLE_IS_PARKING: {
    message: 'cant_edit_for_vehicle_is_parking',
    code: '22000',
  },
  HASH_ERROR: {
    message: 'not_match_hash',
    code: '23000',
  },
  EMAIL_ERROR: {
    message: 'cannot_send_email',
    code: '24000',
  },
  INVALID_SESSION: {
    message: 'invalid_session',
    code: '25000',
  },
  TWILIO_ERROR: {
    message: 'twilio_error',
    code: '26000',
  },
  SOCKET_ERROR:{
    message: 'socket_error',
    code: '27000',
  },
}
