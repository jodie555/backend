'use strict'

import { Response, Request, NextFunction } from 'express'
import session from 'express-session'
import jwt from 'jsonwebtoken'
import errorHandler from '../exception/errorHandler'
import { CONSTANT_ERROR, Exception } from '../exception/Exception'
// const jwt = require('jsonwebtoken')
// const errorHandler = require('../exception/errorHandler')
// import User from '../models/user'
import logApiRequest from './logApiRequest'
// const logApiRequest = require('./logApiRequest')
const Mongoose = require("mongoose");



let authUser = null
let user = function () {
  return authUser
}



async function checkApi(req: Request, res: Response, next: NextFunction) {
  try{

    const userId = req.session.auth._id
    let User = Mongoose.model("User");
      
    const doesUserExist = User.exists({_id:userId})

    if(!doesUserExist){
      throw new Exception(
        CONSTANT_ERROR.UNAUTHENTICATED.message,
        CONSTANT_ERROR.UNAUTHENTICATED.code
      )
    }

  }catch(err){
    errorHandler.response(
      new Exception(
        CONSTANT_ERROR.UNAUTHENTICATED.message,
        CONSTANT_ERROR.UNAUTHENTICATED.code
      ).setDetail(err.message),
      req,
      res,
      next
    )
    return false
  }

  next()
}

// async function checkWeb(req: Request, res: Response, next: NextFunction) {
//   let token: string = <string>req.query.token
//   if (!token) {
//     errorHandler.response(
//       new Exception(
//         CONSTANT_ERROR.UNAUTHENTICATED.message,
//         CONSTANT_ERROR.UNAUTHENTICATED.code
//       ),
//       req,
//       res,
//       next
//     )
//     return false
//   }
//   const secret = process.env.AUTH_JWT_KEY
//   try {
//     var decoded = jwt.verify(token, secret)
//     authUser = await User.findByPk(decoded['id'])
//     if (authUser) {
//       logApiRequest.logUser(req, authUser)
//     } else {
//       throw new Exception(
//         CONSTANT_ERROR.UNAUTHENTICATED.message,
//         CONSTANT_ERROR.UNAUTHENTICATED.code
//       )
//     }
//   } catch (err) {
//     errorHandler.response(
//       new Exception(
//         CONSTANT_ERROR.UNAUTHENTICATED.message,
//         CONSTANT_ERROR.UNAUTHENTICATED.code
//       ).setDetail(err.message),
//       req,
//       res,
//       next
//     )
//     return false
//   }
//   next()
// }

//  MUST be put in last of this file due to silly JS compile sequence
export default {
  api: checkApi,
  user: user,
  // web: checkWeb,
}
