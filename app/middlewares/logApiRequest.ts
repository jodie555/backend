'use strict'

import { Response, Request, NextFunction } from 'express'
import logger from '../logger'
import { getLocale, setLocale } from 'i18n'

export default {
  log: log,
  logUser: logUser,
}

function log(req: Request, res: Response, next: NextFunction) {
  setLocale(res, global['locale'])

  let url = req.originalUrl
  let message =
    'Request::[' +
    req.ip +
    ']-[' +
    req.method +
    ']-[' +
    url +
    ']-[' +
    req.hostname +
    ']-[' +
    req.get('accept-language') +
    ']-[' +
    getLocale() +
    ']'
  if (url == '/login') {
    let username = req.body.username
    message = message + '-[' + username + ']'
  }
  logger.info(message)
  next()
}

function logUser(req: Request, user) {
  let url = req.originalUrl
  let message =
    'Request::[' +
    req.ip +
    ']-[' +
    req.method +
    ']-[' +
    url +
    ']-[' +
    req.hostname +
    ']-[' +
    user.username +
    ']'
  logger.info(message)
}
