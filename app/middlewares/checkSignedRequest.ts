'use strict'

import { NextFunction, Request, Response } from 'express'
import moment from 'moment-timezone'
import { CONSTANT_ERROR, Exception } from '../exception/Exception'
import { Md5 } from 'md5-typescript'

async function checkSign(req: Request, res: Response, next: NextFunction) {
  let requestTime = req.header('request-time')
  if (
    !requestTime ||
    !moment(requestTime).isValid() ||
    moment(requestTime).diff(moment(), 'minutes') > 15
  ) {
    throw new Exception(
      CONSTANT_ERROR.REQUEST_EXPIRED.message,
      CONSTANT_ERROR.REQUEST_EXPIRED.code
    )
  }
  let nonce = req.header('nonce')
  if (!nonce) {
    throw new Exception(
      CONSTANT_ERROR.REQUEST_WITHOUT_NONCE.message,
      CONSTANT_ERROR.REQUEST_WITHOUT_NONCE.code
    )
  }
  let appId = req.header('app-id')
  if (appId != process.env.APP_ID) {
    throw new Exception(
      CONSTANT_ERROR.APPLICATION_NOT_FOUND.message,
      CONSTANT_ERROR.APPLICATION_NOT_FOUND.code
    )
  }
  let secret = req.header('secret')
  let appSecret = process.env.APP_SECRET
  let salt = process.env.APP_SALT
  let newSecret = Md5.init(appSecret + salt + requestTime + nonce)
  if (secret != newSecret) {
    throw new Exception(
      CONSTANT_ERROR.REQUEST_UNSIGNED.message,
      CONSTANT_ERROR.REQUEST_UNSIGNED.code
    )
  }
  next()
}

export default checkSign
