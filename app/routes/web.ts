'use strict'

import express from 'express'
import fs from 'fs'
import bodyParser from 'body-parser' //  Parse JSON body
const router = express.Router()
router.use(bodyParser.json())

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: process.env.APP_NAME })
})

export default router
// module.exports = router
