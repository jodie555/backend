'use strict'

import apiRouter from './routes'
import webRouter from './web'

export default {
  applyRoutes: applyRoutes,
}

//Accept  Content-Type  application/json
function applyRoutes(req, res, next) {
  // // console.log('applyRoutes1')
  // if (req.accepts('html')) {
  //   console.log('html')
  //   webRouter(req, res, next)
  // } else if (req.accepts('json')) {
  //   console.log('api')
  //   apiRouter(req, res, next)
  // }
  apiRouter(req, res, next)

}
