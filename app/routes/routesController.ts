const Glob = require("glob");
const Path = require("path");



export async function routesController(router) {
    // let files = Glob.sync("*/api/login/routes.js");
    // let files = Glob.sync("**/api/*/routes.js");

    let files = Glob.sync("**/api/*/routes.ts");


    console.log(files)
    for (let file of files) {
        console.log(`Requiring routes`, file);
        let routeController = require(Path.resolve(file));
        routeController(router);
    }
    console.log("finished")


}

