const Glob = require("glob");
const Path = require("path");
import{ routesController }from './routesController'
"use strict";

import { Exception, CONSTANT_ERROR } from "../exception/Exception";
import express from "express";
import bodyParser from "body-parser";

import checkSign from "../middlewares/checkSignedRequest";
// app.use(checkSign)

const router = express.Router();
// router.use(checkSign);
router.use(bodyParser.json({ limit: "64mb" }));
router.use(bodyParser.urlencoded({ limit: "64mb", extended: true }));
// global.setRouteMacro(router);

routesController(router)

router.all("/*", function (req, res, next) {
  throw new Exception(
    CONSTANT_ERROR.NOT_FOUND.message,
    CONSTANT_ERROR.NOT_FOUND.code
  ).setDetail(`path to ${req.url} is not found.`);
});
export default router;
// module.exports = router



