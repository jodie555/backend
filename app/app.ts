'use strict'

import express, { Application, Request, Response, NextFunction } from 'express'
require('dotenv').config() // The .Env file in the root directory of the project is read by default
require('express-async-errors')



//init database
const initDatabase = require("./database");
const Config = require("./config/mongoose");
let { MONGO } = Config;
let { USERNAME, PASSWORD, HOST, PORT, DBNAME, AUTHDB } = MONGO;
// const MONGODB_URI = `mongodb+srv://${USERNAME}:${PASSWORD}@cluster0-zkhem.mongodb.net/${DBNAME}?retryWrites=true&w=majority`
// const MONGODB_URI = `mongodb+srv://${USERNAME}:${PASSWORD}@cluster0.ro1b9.mongodb.net/${DBNAME}?retryWrites=true&w=majority`
const MONGODB_URI = `mongodb+srv://${USERNAME}:${PASSWORD}@cluster0.7rde7.mongodb.net/${DBNAME}?retryWrites=true&w=majority`
initDatabase.Database(MONGODB_URI)


// var session = require('express-session')
import session from 'express-session'
const MongoDBStore = require('connect-mongodb-session')(session)

import createError from 'http-errors'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import cors from 'cors'
import i18n from 'i18n'
import moment from 'moment-timezone'

// import './db'


declare module 'express-session' {
  export interface SessionData {
    auth: { [key: string]: any };
    roomName: {[key: string]: any};
    zoom_auth:{[key: string]: any};
  }
}
let app = express()
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection:"sessions",
})




// // view engine setup
// app.set('views', path.join(__dirname, '/../views'))
// app.set('view engine', 'pug')

// app.use(logger('dev'))
app.use(express.json({limit: '50mb'}))

// app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

//public file
app.use(express.static(path.join(__dirname, '/../public')))

app.use(cors({

  origin: 'http://localhost:3000',
  // origin: 'https://justaskfrontendtest1.herokuapp.com',
  // origin: 'https://justask-frontend.herokuapp.com',





  credentials: true,
  exposedHeaders: ['set-cookie']
})) //Allow cross domain

moment.tz.setDefault('Asia/Hong_Kong')

// i18n Config (Multi-language support)
i18n.configure({
  locales: ['zh-TW', 'en', 'zh-CN'],
  defaultLocale: 'zh-TW',
  header: 'accept-language',
  directory: path.join(__dirname + '/../i18n'),
  register: global,
})
app.use(i18n.init) // default: using 'accept-language' header to guess language settings

// app.set('trust proxy', 1)
app.use(session({ secret: 'dfwgweherfdgdshdh',
//  cookie: { 
//   //  maxAge: 60000,
//   // secure: false,
//    secure: true,
//    httpOnly: true,
//    sameSite: 'none',
//    maxAge: 60 * 60 * 24 * 1
//   //  maxAge: 60 * 60 * 1

// },
  cookie: { 


    maxAge: 60 * 60 * 24 * 10

  },
 resave: true,
 saveUninitialized: true,
 store:store
}))



// //  Set up middleware
// import logApiRequest from './middlewares/logApiRequest'
// app.use(logApiRequest.log)

// // require util dir and custom global module
// import './autoload'



//  Set request timeout
app.use(function (req, res, next) {
  req.setTimeout(50000)
  next()
})

import { Exception, CONSTANT_ERROR } from './exception/Exception'
import { __ } from 'i18n'
app.get('/',function (req, res) {
  // throw new Exception(
  //   CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
  //   CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
  // ).setDetail(__('empty_username'))
  res.send('hello worldf')

})
//  Routers
import router from './routes/router'
// const router = require('./routes/router')
app.use(router.applyRoutes)

console.log("router",router)


// const router = express.Router();
// router.get("/", function (req, res, next) {
//   console.log("ddd")
//   res.json({ result: "success", data: true });
// });
// app.use(router)




// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// general error handler for non-catching exceptions
import errorHandler from './exception/errorHandler'
app.use(errorHandler.response)

export default app
// module.exports = app
