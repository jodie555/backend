import { check, body, param } from 'express-validator'
import { __ } from 'i18n'
import { validate } from './index'

export const checkCreate = [
  body('username').notEmpty().withMessage('username_require'),
  body('password')
    .notEmpty()
    .isLength({ min: 6 })
    .withMessage('password_check_failed'),
  body('role').notEmpty().isIn(['a', 'b', 'o']),
  validate,
]

export const checkUpdate = [
  body('username').notEmpty().withMessage('username_require'),
  // body('password')
  //   .notEmpty()
  //   .isLength({ min: 6 })
  //   .withMessage('password_check_failed'),
  // body('role').notEmpty().isIn(['a', 'b', 'o']),
  validate,
]
