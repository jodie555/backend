import { query } from 'express'
import { check, body, param } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkCreate = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('start_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('end_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('payment_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('parking_lot').notEmpty().withMessage('check_failed'),
  body('name').notEmpty().withMessage('check_failed'),
  body('mobile').notEmpty().withMessage('check_failed'),
  body('remarks').exists().withMessage('check_failed'),
  body('transaction_info').isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .default(0)
    .isDecimal()
    .withMessage('check_failed'),

  body('transaction_info.payable_amount')
    .isDecimal()
    .notEmpty()
    .withMessage('check_failed'),
  body('transaction_info.trace_number').notEmpty().withMessage('check_failed'),
  validate,
]

export const checkUpdate = [
  param('monthly_record_id').notEmpty().withMessage('require'),
  body('plate_number')
    .toUpperCase()
    .customSanitizer((value) => {
      if (value) {
        return value.replace(/\s+/g, '')
      }
      return value
    }),
  body('start_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('end_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('payment_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('parking_lot').notEmpty().withMessage('check_failed'),
  body('name').notEmpty().withMessage('check_failed'),
  body('mobile').notEmpty().withMessage('check_failed'),
  body('remarks').exists().withMessage('check_failed'),
  body('transaction_info').isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .default(0)
    .isDecimal()
    .withMessage('check_failed'),

  body('transaction_info.payable_amount')
    .isDecimal()
    .notEmpty()
    .withMessage('check_failed'),
  body('transaction_info.trace_number').notEmpty().withMessage('check_failed'),
  validate,
]

export const checkVoidMonthlyRecord = [
  param('monthly_record_id').notEmpty().withMessage('require'),
  validate,
]

export const checkRenewalOfContract = [
  param('monthly_record_id').notEmpty().withMessage('require'),
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('start_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('end_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('payment_date')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('parking_lot').notEmpty().withMessage('check_failed'),
  body('name').notEmpty().withMessage('check_failed'),
  body('mobile').notEmpty().withMessage('check_failed'),
  body('transaction_info').isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .isDecimal()
    .withMessage('check_failed'),
  body('transaction_info.payable_amount')
    .isDecimal()
    .notEmpty()
    .withMessage('check_failed'),
  body('transaction_info.trace_number').notEmpty().withMessage('check_failed'),
  validate,
]
