'use strict'
import { check, body, param, query } from 'express-validator'
import { __, getLocale } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'
export const checkOtherRecordCreate = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('datetime')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('incident_type').isIn(['accident', 'other']).withMessage('check_failed'),
  body('description').notEmpty().withMessage('check_failed'),
  body('transaction_info').isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.payable_amount')
    .isDecimal()
    .notEmpty()
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .default(0)
    .isDecimal()
    .withMessage('check_failed'),
  body('transaction_info.trace_number').notEmpty().withMessage('check_failed'),
  validate,
]

export const checkOtherRecordUpdate = [
  param('other_record_id').notEmpty().withMessage('require'),
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('datetime')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('incident_type').isIn(['accident', 'other']).withMessage('check_failed'),
  body('description').notEmpty().withMessage('check_failed'),
  body('transaction_info').isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.payable_amount')
    .isDecimal()
    .notEmpty()
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .default(0)
    .isDecimal()
    .withMessage('check_failed'),
  body('transaction_info.trace_number').notEmpty().withMessage('check_failed'),
]

export const checkDeleteOtherRecord = [
  param('other_record_id').notEmpty().withMessage('require'),
]
