import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkCreatePriceScheme = [
  body('time_unit').notEmpty().withMessage('check_failed'),
  body('plate_type')
    .notEmpty()
    .isIn(['p', 'm', 'd'])
    .withMessage('check_failed'),
  body('remarks').exists(),
  validate,
]

export const checkUpdatePriceScheme = [
  body('time_unit').notEmpty().withMessage('check_failed'),
  body('plate_type')
    .notEmpty()
    .isIn(['p', 'm', 'd'])
    .withMessage('check_failed'),
  body('remarks').exists(),
  validate,
]

export const checkCreatePriceSession = [
  body('start_time').notEmpty().withMessage('check_failed'),
  body('end_time').notEmpty().withMessage('check_failed'),
  body('rate_per_time_unit').notEmpty().withMessage('check_failed'),
  body('max_rate_per_session').notEmpty().withMessage('check_failed'),
  body('grace_period_per_time_unit').notEmpty().withMessage('check_failed'),
  body('grace_period_per_session_unit').notEmpty().withMessage('check_failed'),
  body('remarks').notEmpty().withMessage('check_failed'),
  validate,
]

export const checkUpdatePriceSession = [
  body('start_time').notEmpty().withMessage('check_failed'),
  body('end_time').notEmpty().withMessage('check_failed'),
  body('rate_per_time_unit').notEmpty().withMessage('check_failed'),
  body('max_rate_per_session').notEmpty().withMessage('check_failed'),
  body('grace_period_per_time_unit').notEmpty().withMessage('check_failed'),
  body('grace_period_per_session_unit').notEmpty().withMessage('check_failed'),
  body('remarks').notEmpty().withMessage('check_failed'),
  validate,
]
