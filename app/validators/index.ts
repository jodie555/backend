import { NextFunction, Request, Response } from 'express'
import { validationResult, matchedData } from 'express-validator'
import { CONSTANT_ERROR, Exception } from '../exception/Exception'
import { __ } from 'i18n'

export function validate(req: Request, res: Response, next: NextFunction) {
  let errors = validationResult(req)
  if (!errors.isEmpty()) {
    let msg = ''
    let error = errors.mapped()
    Object.keys(error).forEach((key) => {
      msg += `${error[key].param}:${__(error[key].msg)};`
    })
    throw new Exception(
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.message,
      CONSTANT_ERROR.PARAM_VALIDATION_ERROR.code
    ).setDetail(msg)
  }
  next()
}

export function getMatchedData(req: Request, res: Response) {


  let queryData = matchedData(req, { locations: ['query'] })
  let bodyData = matchedData(req, { locations: ['body'] })
  let paramData = matchedData(req, { locations: ['params'] })
  let data = matchedData(req)


  return {
    query_data: queryData,
    body_data: bodyData,
    param_data: paramData,
    data: data,
  }
}
