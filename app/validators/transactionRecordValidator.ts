import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkUpdate = [
  body('payment_method').exists().withMessage('check_failed'),
  body('trace_number').exists().withMessage('check_failed'),
  body('remarks').exists().withMessage('check_failed'),
  body('payable_amount')
  .custom(function (value) {
    return true
  })
  .withMessage('check_failed'),
  body('discount_amount').exists().withMessage('check_failed'),
  validate,
]

export const checkSettlement = [
  body('transaction_record_ids')
    .notEmpty()
    .isArray()
    .withMessage('check_failed'),
  validate,
]
