import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkCommonVehicle = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  validate,
]
