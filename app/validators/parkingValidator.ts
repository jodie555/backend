import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkEnterGate = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('plate_type')
    .notEmpty()
    .isIn(['p', 'd', 'm'])
    .withMessage('check_failed'),
  validate,
]

export const checkOutGate = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('out_time')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
  body('transaction_info').default({}).isObject().withMessage('check_failed'),
  body('transaction_info.payment_method')
    .default('others')
    .isIn([
      'cash',
      'octopus',
      'alipay',
      'wechat',
      'check',
      'visa-master',
      'ae',
      'others',
    ])
    .withMessage('check_failed'),
  body('transaction_info.trace_number')
    .default(null)
    .exists()
    .withMessage('check_failed'),
  body('transaction_info.discount_amount')
    .default(0)
    .isDecimal()
    .withMessage('check_failed'),
  validate,
]

export const checkGetOutGateInfo = [
  query('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  query('out_time')
    .custom((el) => {
      return moment(el).isValid()
    })
    .withMessage('check_failed'),
]

export const checkGetParkingVehicle = [
  param('plate_number')
    .notEmpty()
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
]

export const checkUpdate = [
  body('plate_number')
    .notEmpty()
    .withMessage('require')
    .toUpperCase()
    .customSanitizer((value) => {
      return value.replace(/\s+/g, '')
    }),
  body('plate_type')
    .notEmpty()
    .isIn(['p', 'd', 'm'])
    .withMessage('check_failed'),
  validate,
]
