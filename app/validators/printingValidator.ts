import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkPrintingEnter = [
  body('plate_number')
    .notEmpty(),
  body('enter_time')
    .notEmpty(),
  body('index')
  .notEmpty(),
  body('re_print')
  .notEmpty(),
  validate,
]

export const checkPrintingExit = [
  body('plate_number')
    .notEmpty(),
  body('enter_time')
    .notEmpty(),
  body('index')
  .notEmpty(),
  body('format_period')
  .notEmpty(),
  body('payable_amount')
  .notEmpty(),
  body('out_time')
  .notEmpty(),
  body('re_print')
  .notEmpty(),
  validate,
]


export const checkPrintingBreakDown = [
  body('amount_statistics')
    .notEmpty(),
  body('settlement_record')
    .notEmpty(),
  validate,
]