import { check, body, param, query } from 'express-validator'
import { __ } from 'i18n'
import moment from 'moment-timezone'
import { validate } from './index'

export const checkGpioStatus = [
  body('status').notEmpty().isIn(['on', 'off']).withMessage('require'),
  validate,
]
