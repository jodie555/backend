'use strict'

const winston = require('winston')
const MESSAGE = Symbol.for('message')
import moment from 'moment-timezone'

moment.tz.setDefault('Asia/Hong_Kong')
let today = moment().format('YYYY-MM-DD')
const jsonFormatter = (logEntry) => {
  const base = {
    time: moment().format('YYYY-MM-DD H:m:s'),
  }
  const json = Object.assign(base, logEntry)
  logEntry[MESSAGE] = JSON.stringify(json)
  return logEntry
}

const logger = new winston.createLogger({
  format: winston.format(jsonFormatter)(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({
      filename: `./storage/logs/${process.env.APP_NAME}-${today}.log`,
      prettyPrint: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
    }),
  ],
})

export default logger
// module.exports = logger
